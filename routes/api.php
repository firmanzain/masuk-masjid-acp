<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'prefix' => 'v1'
], function () {
    // Route::group([
    //         'prefix' => 'adhan'
    // ], function () {
    //   Route::get('aladhan', 'MosqueController@getAladhan');
    // });
    // Route::get('test', 'MosqueController@test');
    Route::group([
            'prefix' => 'acp'
    ], function () {
        Route::post('login', 'TakmirController@login');
        Route::get('image/background/file/{background}', 'ImageBackgroundController@image')->name('background.image');
        // ACP ROUTE
        Route::group([
                'middleware' => ['auth:api']
        ], function () {
            Route::get('check', 'TakmirController@check');
            Route::post('logout', 'TakmirController@logout');
            Route::group([
                    'prefix' => 'province'
            ], function () {
                Route::get('select', 'ProvinceController@select');
            });
            Route::group([
                    'prefix' => 'province'
            ], function () {
                Route::get('select', 'ProvinceController@select');
            });
            Route::post('mosque/{mosque}', 'MosqueController@update')->name('mosque.update');
            Route::post('mosque-status/{mosque}', 'MosqueController@updateStatus')->name('mosque.updateStatus');
            Route::resource('takmir', 'TakmirController')->only([
              'index', 'store', 'update', 'destroy'
            ]);
            Route::put('device/verification/{device}', 'DeviceController@verification')->name('device.verification');
            Route::get('device/otp/regenerate/{device}', 'DeviceController@otpRegenerate')->name('device.otp.regenerate');
            Route::put('device/status/{device}', 'DeviceController@status')->name('device.status');
            Route::resource('device', 'DeviceController')->only([
              'index', 'update', 'destroy'
            ]);
            Route::put('preacher/status/{preacher}', 'PreacherController@status')->name('preacher.status');
            Route::resource('preacher', 'PreacherController')->only([
              'index', 'store', 'update', 'destroy'
            ]);
            Route::put('donature/status/{donature}', 'DonatureController@status')->name('donature.status');
            Route::resource('donature', 'DonatureController')->only([
              'index', 'store', 'update', 'destroy'
            ]);
            Route::put('event/status/{event}', 'EventController@status')->name('event.status');
            Route::resource('event', 'EventController')->only([
              'index', 'store', 'update', 'destroy'
            ]);
            Route::group([
                    'prefix' => 'setting'
            ], function () {
                Route::resource('general', 'PrayerDurationController')->only([
                    'index', 'update'
                ]);
            });
            Route::group([
                    'prefix' => 'text'
            ], function () {
                Route::put('running/status/{running}', 'TextRunningController@status')->name('running.status');
                Route::resource('running', 'TextRunningController')->only([
                    'index', 'store', 'update', 'destroy'
                ]);
                Route::put('info/status/{info}', 'TextInfoController@status')->name('info.status');
                Route::resource('info', 'TextInfoController')->only([
                    'index', 'store', 'update', 'destroy'
                ]);
                Route::put('arabic/status/{arabic}', 'TextArabicController@status')->name('arabic.status');
                Route::resource('arabic', 'TextArabicController')->only([
                    'index', 'store', 'update', 'destroy'
                ]);
            });
            Route::group([
                    'prefix' => 'image'
            ], function () {
                Route::put('background/status/{background}', 'ImageBackgroundController@status')->name('background.status');
                Route::post('background/update/{background}', 'ImageBackgroundController@update')->name('background.update');
                Route::resource('background', 'ImageBackgroundController')->only([
                    'index', 'store', 'destroy'
                ]);
                Route::put('photo/status/{photo}', 'ImagePhotoController@status')->name('photo.status');
                Route::post('photo/update/{photo}', 'ImagePhotoController@update')->name('photo.update');
                Route::resource('photo', 'ImagePhotoController')->only([
                    'index', 'store', 'destroy'
                ]);
            });
        });
    });
    // DEVICE ROUTE
    Route::group([
            'prefix' => 'device'
    ], function () {
        Route::post('register', 'DeviceController@register');
        Route::post('login', 'DeviceController@login');
        Route::group([
            'middleware' => ['auth:device'],
        ], function () {
            Route::get('', 'DeviceController@getData');
            Route::post('logout', 'DeviceController@logout');
            Route::post('version/update', 'DeviceController@updateVersion');
            Route::get('adhan/{month?}/{year?}', 'DeviceController@getDataAdhan');
            Route::get('hijriah/{month?}/{year?}', 'DeviceController@getDataHijriah');
        });
    });
    // TEAM ROUTE
    Route::group([
            'prefix' => 'team'
    ], function () {
        Route::post('login', 'TeamController@login');
        Route::group([
                'middleware' => ['auth:team']
        ], function () {
            Route::get('check', 'TeamController@check');
            Route::post('logout', 'TeamController@logout');
            Route::post('account/{account}', 'TeamController@updateaccount')->name('team.updateaccount');
            Route::group([
                    'prefix' => 'bank'
            ], function () {
                Route::get('select', 'BankController@select');
            });
            Route::get('dashboard/count', 'TeamController@dashboardCount')->name('team.dashboardcount');
            Route::get('history', 'TeamController@history')->name('team.history');
            Route::resource('withdraw', 'TeamWithdrawController')->only([
                'store',
            ]);
        });
    });
});
