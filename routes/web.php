<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::redirect('/', '/login');
Route::redirect('/register', '/login');
Route::redirect('/password/reset', '/login');

Route::group(['middleware' => ['auth']], function () {
    Route::get('/package/select', 'PackageController@select')->name('package.select');
    Route::resource('/package', 'PackageController');
    Route::resource('/mosque', 'MosqueController');
    Route::get('/province/select', 'ProvinceController@select')->name('province.select');
    Route::put('/mosque/{mosque}', 'MosqueController@updateacp')->name('mosque.updateacp');
    Route::post('/mosque/renew-subscribe/{mosque}', 'MosqueController@renewsubscribe')->name('mosque.renewsubscribe');
    Route::resource('/bank', 'BankController');
    Route::get('/team/select', 'TeamController@select')->name('team.select');
    Route::resource('/team', 'TeamController');
    Route::post('/withdraw/accept/{withdraw}', 'TeamWithdrawController@accept')->name('team.accept');
    Route::post('/withdraw/decline/{withdraw}', 'TeamWithdrawController@decline')->name('team.decline');
    Route::resource('withdraw', 'TeamWithdrawController')->only([
      'index', 'delete'
    ]);
});

Route::get('/{vue_capture?}', function () {
    return view('layouts.app');
})->where('vue_capture', '[\/\w\.-]*');
