<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSolatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solats', function (Blueprint $table) {
            $table->string('id');
            $table->unsignedBigInteger('mosque_id');
            $table->string('name');
            $table->dateTime('dateTime');
            $table->boolean('isAdhan')->default(1);
            $table->timestamps();
            $table->unique(['id', 'mosque_id', 'name', 'dateTime']);
            $table->foreign('mosque_id')->references('id')->on('mosques')
                  ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solats');
    }
}
