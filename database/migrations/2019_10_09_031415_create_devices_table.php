<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('devices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('mosque_id');
            $table->uuid('number');
            $table->string('name');
            $table->integer('location')->default(1)->comment('1: Inside, 2: Outside');
            $table->integer('status_confirmation')->default(1)->comment('0: Decline, 1: Request, 2: Accept');
            $table->integer('otp')->nullable();
            $table->dateTime('otp_valid')->nullable();
            $table->dateTime('last_sync')->nullable();
            $table->integer('status')->default(1)->comment('1:Active, 0:Non Active');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('mosque_id')->references('id')->on('mosques')
                  ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('devices');
    }
}
