<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrayerDurationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prayer_durations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('mosque_id');
            $table->enum('name', ['subuh', 'zuhur', 'asar', 'magrib', 'isya', 'jumat']);
            $table->string('start_at')->default('00:00:00');
            $table->string('azan_duration')->default('00:00:00');
            $table->string('iqamat_duration')->default('00:00:00');
            $table->string('solat_duration')->default('00:00:00');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('mosque_id')->references('id')->on('mosques')
                  ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prayer_durations');
    }
}
