<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDonaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('donatures', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('mosque_id');
            $table->date('date');
            $table->string('name');
            $table->text('address')->nullable();
            $table->integer('amount')->default(0);
            $table->text('information')->nullable();
            $table->integer('status')->default(1)->comment('1:Active, 0:Non Active');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('mosque_id')->references('id')->on('mosques')
                  ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('donatures');
    }
}
