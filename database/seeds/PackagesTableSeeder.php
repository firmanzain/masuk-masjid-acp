<?php

use Illuminate\Database\Seeder;

class PackagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('packages')->insert([
            'count' => 1,
            'price' => 90000,
        ]);
        DB::table('packages')->insert([
            'count' => 2,
            'price' => 150000,
        ]);
        DB::table('packages')->insert([
            'count' => 3,
            'price' => 200000,
        ]);
        DB::table('packages')->insert([
            'count' => 0,
            'price' => 250000,
        ]);
    }
}
