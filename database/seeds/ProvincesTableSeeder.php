<?php

use Illuminate\Database\Seeder;

class ProvincesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('provinces')->insert([
            'code' => 11,
            'name' => 'ACEH',
        ]);
        DB::table('provinces')->insert([
            'code' => 12,
            'name' => 'SUMATERA UTARA',
        ]);
        DB::table('provinces')->insert([
            'code' => 13,
            'name' => 'SUMATERA BARAT',
        ]);
        DB::table('provinces')->insert([
            'code' => 14,
            'name' => 'RIAU',
        ]);
        DB::table('provinces')->insert([
            'code' => 15,
            'name' => 'JAMBI',
        ]);
        DB::table('provinces')->insert([
            'code' => 16,
            'name' => 'SUMATERA SELATAN',
        ]);
        DB::table('provinces')->insert([
            'code' => 17,
            'name' => 'BENGKULU',
        ]);
        DB::table('provinces')->insert([
            'code' => 18,
            'name' => 'LAMPUNG',
        ]);
        DB::table('provinces')->insert([
            'code' => 19,
            'name' => 'KEPULAUAN BANGKA BELITUNG',
        ]);
        DB::table('provinces')->insert([
            'code' => 21,
            'name' => 'KEPULAUAN RIAU',
        ]);
        DB::table('provinces')->insert([
            'code' => 31,
            'name' => 'DKI JAKARTA',
        ]);
        DB::table('provinces')->insert([
            'code' => 32,
            'name' => 'JAWA BARAT',
        ]);
        DB::table('provinces')->insert([
            'code' => 33,
            'name' => 'JAWA TENGAH',
        ]);
        DB::table('provinces')->insert([
            'code' => 34,
            'name' => 'DI YOGYAKARTA',
        ]);
        DB::table('provinces')->insert([
            'code' => 35,
            'name' => 'JAWA TIMUR',
        ]);
        DB::table('provinces')->insert([
            'code' => 36,
            'name' => 'BANTEN',
        ]);
        DB::table('provinces')->insert([
            'code' => 51,
            'name' => 'BALI',
        ]);
        DB::table('provinces')->insert([
            'code' => 52,
            'name' => 'NUSA TENGGARA BARAT',
        ]);
        DB::table('provinces')->insert([
            'code' => 53,
            'name' => 'NUSA TENGGARA TIMUR',
        ]);
        DB::table('provinces')->insert([
            'code' => 61,
            'name' => 'KALIMANTAN BARAT',
        ]);
        DB::table('provinces')->insert([
            'code' => 62,
            'name' => 'KALIMANTAN TENGAH',
        ]);
        DB::table('provinces')->insert([
            'code' => 63,
            'name' => 'KALIMANTAN SELATAN',
        ]);
        DB::table('provinces')->insert([
            'code' => 64,
            'name' => 'KALIMANTAN TIMUR',
        ]);
        DB::table('provinces')->insert([
            'code' => 65,
            'name' => 'KALIMANTAN UTARA',
        ]);
        DB::table('provinces')->insert([
            'code' => 71,
            'name' => 'SULAWESI UTARA',
        ]);
        DB::table('provinces')->insert([
            'code' => 72,
            'name' => 'SULAWESI TENGAH',
        ]);
        DB::table('provinces')->insert([
            'code' => 73,
            'name' => 'SULAWESI SELATAN',
        ]);
        DB::table('provinces')->insert([
            'code' => 74,
            'name' => 'SULAWESI TENGGARA',
        ]);
        DB::table('provinces')->insert([
            'code' => 75,
            'name' => 'GORONTALO',
        ]);
        DB::table('provinces')->insert([
            'code' => 76,
            'name' => 'SULAWESI BARAT',
        ]);
        DB::table('provinces')->insert([
            'code' => 81,
            'name' => 'MALUKU',
        ]);
        DB::table('provinces')->insert([
            'code' => 82,
            'name' => 'MALUKU UTARA',
        ]);
        DB::table('provinces')->insert([
            'code' => 91,
            'name' => 'PAPUA',
        ]);
        DB::table('provinces')->insert([
            'code' => 91,
            'name' => 'PAPUA BARAT',
        ]);
    }
}
