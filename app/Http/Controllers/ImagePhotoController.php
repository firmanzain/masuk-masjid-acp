<?php

namespace App\Http\Controllers;

use App\Models\ImagePhoto;
use Illuminate\Http\Request;
use App\Transformers\Serializer\ArraySerializer;
use App\Transformers\ImagePhotoTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ImagePhotoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $takmir = Auth::user();
        $mosque_id = $takmir->mosque_id;

        if ($request->input('acp') == 'masjid') {
          $columns = ['', 'image', 'name', 'start_at', 'end_at', 'status'];

          $length = $request->input('length');
          $column = $request->input('column');
          $dir = $request->input('dir');
          $searchValue = $request->input('search');

          $query = ImagePhoto::orderBy($columns[$column], $dir)
                                    ->where('mosque_id', $mosque_id);

          if ($searchValue) {
            $query->where(function($query) use ($searchValue) {
              $query->where('image', 'like', '%' . $searchValue . '%')
                    ->orWhere('name', 'like', '%' . $searchValue . '%')
                    ->orWhere('start_at', 'like', '%' . $searchValue . '%')
                    ->orWhere('end_at', 'like', '%' . $searchValue . '%')
                    ->orWhere('status', 'like', '%' . $searchValue . '%');
            });
          }

          $data = $query->paginate($length);
          return ['data' => $data, 'draw' => $request->input('draw')];
        } else {
          return [];
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $takmir = Auth::user();

        $this->validate($request, [
            'name' => 'required|string|max:255',
            'image' => 'required|image|max:1024',
            'information' => 'nullable|string',
            'start_at' => 'required|date|max:255',
            'end_at' => 'required|date|max:255',
        ]);

        $path = $request->file('image')->storeAs('public/images/background', Str::random(10).time().'.'.$request->file('image')->getClientOriginalExtension());

        return ImagePhoto::create([
            'mosque_id' => $takmir->mosque_id,
            'name' => $request['name'],
            'image' => env('APP_URL').'/storage/'.str_replace('public/', '', $path),
            'information' => $request['information'],
            'start_at' => $request['start_at'],
            'end_at' => $request['end_at'],
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ImagePhoto  $imagePhoto
     * @return \Illuminate\Http\Response
     */
    public function show(ImagePhoto $imagePhoto)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ImagePhoto  $imagePhoto
     * @return \Illuminate\Http\Response
     */
    public function edit(ImagePhoto $imagePhoto)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ImagePhoto  $imagePhoto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = ImagePhoto::findOrFail($id);
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'image' => 'nullable|image|max:1024',
            'information' => 'nullable|string',
            'start_at' => 'required|date|max:255',
            'end_at' => 'required|date|max:255',
        ]);

        if ($request->hasFile('image')) {
            $path = $request->file('image')->storeAs('public/images/background', Str::random(10).time().'.'.$request->file('image')->getClientOriginalExtension());
            $data->image = env('APP_URL').'/storage/'.str_replace('public/', '', $path);
        }
        $data->name = $request->input('name');
        $data->information = $request->input('information');
        $data->start_at = $request->input('start_at');
        $data->end_at = $request->input('end_at');
        $data->save();

        return ['message' => 'Data updated!'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ImagePhoto  $imagePhoto
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = ImagePhoto::findOrFail($id);
        $data->delete();
        return ['message' => 'Data Deleted!'];
    }

    public function status(Request $request, $id)
    {
        $data = ImagePhoto::findOrFail($id);
        $this->validate($request, [
            'status' => 'required|integer|max:2',
        ]);

        $data->status = $request->input('status');
        $data->save();
        return ['message' => 'Data updated!'];
    }
}
