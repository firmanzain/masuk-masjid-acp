<?php

namespace App\Http\Controllers;

use App\Models\Package;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Mosque;
use App\Transformers\Serializer\ArraySerializer;
use App\Transformers\PackageTransformer;

class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['package'] = Package::get();

        return view('package', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = array();

        return view('package-form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $package = new Package;
        $package->name = $request->post('name');
        $package->count = intval(str_replace(',', '', $request->post('count')));
        $package->price = intval(str_replace(',', '', $request->post('price')));
        $package->month = intval(str_replace(',', '', $request->post('month')));
        $package->save();

        return redirect('package')->with([
            'alert' => 'alert-success',
            'message' => 'Data has been saved.'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function show(Package $package)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function edit(Package $package)
    {
        $data['package'] = Package::find($package->id);

        return view('package-form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Package $package)
    {
        // $package = Package::find($id);
        // if (!$package) {
        //     return redirect('package')->with([
        //         'alert' => 'alert-danger',
        //         'message' => '<strong>Failed!</strong> Data not found.'
        //     ]);
        // }

        $package->name = $request->post('name');
        $package->count = intval(str_replace(',', '', $request->post('count')));
        $package->price = intval(str_replace(',', '', $request->post('price')));
        $package->month = intval(str_replace(',', '', $request->post('month')));
        $package->save();

        return redirect('package')->with([
            'alert' => 'alert-success',
            'message' => 'Data has been saved.'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function destroy(Package $package)
    {
        $package->delete();
        return response()->json([
            'code' => 200,
        ], 200);
    }

    public function select(Request $request)
    {
        $package = Package::orderBy('price', 'ASC')->get();

        return fractal()
               ->collection($package, new PackageTransformer())
               ->serializeWith(new ArraySerializer())
               ->toArray();
    }
}
