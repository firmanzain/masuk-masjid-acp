<?php

namespace App\Http\Controllers;

use App\Models\Team;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Carbon\Carbon;
use App\Models\TeamToken, App\Models\BankAccount;
use Illuminate\Support\Facades\Hash;
use App\Transformers\Serializer\ArraySerializer;
use App\Transformers\TeamTransformer;
use Illuminate\Support\Facades\Auth;
use App\Models\TeamBalance, App\Models\TeamWithdraw, App\Models\Mosque;
use Illuminate\Support\Facades\DB;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['team'] = Team::with(['balance', 'withdraw'])->get();

        return view('team', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = array();

        return view('team-form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'card_id' => 'required|string|max:255',
            'born_place' => 'required|string|max:255',
            'born_date' => 'required|date',
            'email' =>  'required|string|max:255|unique:teams',
            'password' => 'required|string|max:255',
            'occupation' => 'required|string|max:255',
            'image' => 'nullable|image|max:1024',
            'address' => 'required|string',
        ]);

        $date = Carbon::parse($request->post('born_date'));

        $path = $request->file('image')->storeAs('public/images/avatars', Str::random(10).time().'.'.$request->file('image')->getClientOriginalExtension());

        $team = new Team();
        $team->name = $request->post('name');
        $team->card_id = $request->post('card_id');
        $team->born_place = $request->post('born_place');
        $team->born_date = $date->isoFormat('Y-MM-DD');
        $team->email = $request->post('email');
        $team->password = bcrypt($request->post('password'));
        $team->occupation = $request->post('occupation');
        $team->image = env('APP_URL').'/storage/'.str_replace('public/', '', $path);
        $team->address = $request->post('address');
        $team->save();

        return redirect('team')->with([
            'alert' => 'alert-success',
            'message' => 'Data has been saved.'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function show(Team $team)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function edit(Team $team)
    {
        $data['team'] = Team::find($team->id);

        return view('team-form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Team $team)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'card_id' => 'required|string|max:255',
            'born_place' => 'required|string|max:255',
            'born_date' => 'required|date',
            'email' =>  'required|string|max:255|unique:teams,email,'.$team->id,
            'password' => 'nullable|string|max:255',
            'occupation' => 'required|string|max:255',
            'image' => 'nullable|image|max:1024',
            'address' => 'required|string',
        ]);

        $date = Carbon::parse($request->post('born_date'));

        $team->name = $request->post('name');
        $team->card_id = $request->post('card_id');
        $team->born_place = $request->post('born_place');
        $team->born_date = $date->isoFormat('Y-MM-DD');
        $team->email = $request->post('email');
        if ($request->post('password')) {
            $team->password = bcrypt($request->post('password'));
        }
        $team->occupation = $request->post('occupation');
        if ($request->file('image')) {
            $path = $request->file('image')->storeAs('public/images/avatars', Str::random(10).time().'.'.$request->file('image')->getClientOriginalExtension());
            $team->image = env('APP_URL').'/storage/'.str_replace('public/', '', $path);
        }
        $team->address = $request->post('address');
        $team->save();

        return redirect('team')->with([
            'alert' => 'alert-success',
            'message' => 'Data has been saved.'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function destroy(Team $team)
    {
        $team->delete();
        return response()->json([
            'code' => 200,
        ], 200);
    }

    public function login(Request $request)
    {
        if (!$request->input('email')) {
          return response()->json([
              'code' => 400,
              'message' => 'Field `email` required.'
          ], 400);
        }

        if (!$request->input('password')) {
          return response()->json([
              'code' => 400,
              'message' => 'Field `password` required.'
          ], 400);
        }

        $team = Team::where('email', $request->input('email'))->first();
        if (!$team) {
          return response()->json([
              'code' => 404,
              'message' => 'User not found.'
          ], 404);
        }

        if (!Hash::check($request->input('password'), $team->password)) {
          return response()->json([
              'code' => 403,
              'message' => 'Password not match.'
          ], 403);
        }

        $token = $team->createToken('Access Token Team')->accessToken;
        $team->token = $token;

        $teamToken = new TeamToken;
        $teamToken->team_id = $team->id;
        $teamToken->token = $token;
        $teamToken->save();

        return fractal()
               ->item($team, new TeamTransformer(['token']))
               ->serializeWith(new ArraySerializer())
               ->toArray();
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        $token = $request->bearerToken();
        $teamToken = TeamToken::where('token', $token)->delete();

        return response()->json([
          'code' => 200,
          'message' => $request->bearerToken()
        ], 200);
    }

    public function check(Request $request)
    {
        $data = Auth::user();
        $data->token = $request->bearerToken();

        return fractal()
               ->item($data, new TeamTransformer(['token']))
               ->serializeWith(new ArraySerializer())
               ->toArray();
    }

    public function select(Request $request)
    {
        $team = Team::orderBy('name', 'ASC')->get();

        return fractal()
               ->collection($team, new TeamTransformer())
               ->serializeWith(new ArraySerializer())
               ->toArray();
    }

    public function updateaccount(Request $request, $id)
    {
        $team = Team::find($id);
        if (!$team) {
          return response()->json([
              'code' => 404,
              'message' => 'Data not found.'
          ], 404);
        }

        if ($request->post('name')) {
            $team->name = $request->post('name');
        }
        if ($request->post('born_place')) {
            $team->born_place = $request->post('born_place');
        }
        if ($request->post('born_date')) {
            $team->born_date = $request->post('born_date');
        }
        if ($request->post('password')) {
            $team->password = bcrypt($request->post('password'));
        }
        if ($request->post('occupation')) {
            $team->occupation = $request->post('occupation');
        }
        if ($request->file('image')) {
            $path = $request->file('image')->storeAs('public/images/avatars', Str::random(10).time().'.'.$request->file('image')->getClientOriginalExtension());
            $team->image = env('APP_URL').'/storage/'.str_replace('public/', '', $path);
        }
        if ($request->post('address')) {
            $team->address = $request->post('address');
        }
        $team->save();

        $bank = BankAccount::where('team_id', $team->id)->first();
        if (!$bank) {
            $bank = new BankAccount();
            $bank->team_id = $team->id;
            $bank->bank_id = $request->post('bank_id');
            $bank->number = $request->post('bank_number');
            $bank->name = $request->post('bank_name');
            $bank->save();
        } else {
            $bank->bank_id = $request->post('bank_id');
            $bank->number = $request->post('bank_number');
            $bank->name = $request->post('bank_name');
            $bank->save();
        }

        return fractal()
               ->item($team, new TeamTransformer())
               ->serializeWith(new ArraySerializer())
               ->toArray();
    }

    public function dashboardCount(Request $request)
    {
        $data = Auth::user();

        $mosque = Mosque::where('team_id', $data->id)->count();

        $balance = TeamBalance::where('team_id', $data->id)->sum('nominal');
        $withdraw = TeamWithdraw::select(DB::raw('SUM(nominal + admin) as nominal'))
                                  ->where('team_id', $data->id)->where('status', '!=', 0)
                                  ->first();

        $count = array(
          'mosque' => $mosque,
          'balance' => $balance - $withdraw->nominal,
        );

        return response()->json([
            'data' => $count,
            'code' => 200,
        ], 200);
    }

    public function history(Request $request)
    {
        $team = Auth::user();

        if ($request->input('acp') == 'team') {

            $length = $request->input('length');
            $column = $request->input('column');
            $dir = $request->input('dir');
            $searchValue = $request->input('search');

            $columns = ['', 'date', 'nominal', 'admin', 'information'];

            $balance = TeamBalance::select(DB::raw('date, nominal, 0 AS admin, information, "" AS status'))
                                    ->where('team_id', $team->id);
            if ($request->input('filter') == 1) {
                $balance->where('information', 'like', '%Baru%');
            }
            if ($request->input('filter') == 2) {
                $balance->where('information', 'like', '%Device%');
            }
            if ($searchValue) {
                $balance->where(function($query) use ($searchValue) {
                    $query->where('date', 'like', '%' . $searchValue . '%')
                    ->orWhere('nominal', 'like', '%' . $searchValue . '%')
                    ->orWhere('information', 'like', '%' . $searchValue . '%');
                });
            }

            $withdraw = TeamWithdraw::select(DB::raw('date, nominal, admin, information, IF(team_withdraws.status = 1, "(Sukses)", IF(team_withdraws.status = 2, "(Pending)", "(Ditolak)")) AS status'))
                                      ->where('team_id', $team->id);
            if ($request->input('filter') != 3) {
                $withdraw->union($balance);
            }
            if ($searchValue) {
                $withdraw->where(function($query) use ($searchValue) {
                    $query->where('date', 'like', '%' . $searchValue . '%')
                    ->orWhere('nominal', 'like', '%' . $searchValue . '%')
                    ->orWhere('information', 'like', '%' . $searchValue . '%');
                });
            }

            if ($request->input('filter') == 0 || $request->input('filter') == 3) {
                $query = $withdraw;
            } else {
                $query = $balance;
            }
            $query->orderBy($columns[$column], $dir);

            $data = $query->paginate($length);
            return ['data' => $data, 'draw' => $request->input('draw')];
        } else {
            return [];
        }
    }
}
