<?php

namespace App\Http\Controllers;

use App\Models\Takmir;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use App\Transformers\Serializer\ArraySerializer;
use App\Transformers\TakmirTransformer;
use Illuminate\Support\Facades\Auth;
use App\Models\TakmirToken, App\Models\Mosque;

class TakmirController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $takmir = Auth::user();
        $mosque_id = $takmir->mosque_id;

        if ($request->input('acp') == 'masjid') {
          $columns = ['', 'name'];

          $length = $request->input('length');
          $column = $request->input('column');
          $dir = $request->input('dir');
          $searchValue = $request->input('search');

          $query = Takmir::orderBy($columns[$column], $dir)
                           ->where('mosque_id', $mosque_id);

          if ($searchValue) {
            $query->where(function($query) use ($searchValue) {
              $query->where('name', 'like', '%' . $searchValue . '%');
            });
          }

          $data = $query->paginate($length);
          return ['data' => $data, 'draw' => $request->input('draw')];
        } else {
          return [];
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $takmir = Auth::user();

        $this->validate($request, [
            'name' => 'required|string|max:255',
            'username' => 'required|string|max:15|unique:takmirs,username,NULL,mosque_id,mosque_id,' .$takmir->mosque_id,
            'password' => 'required|string|max:255',
        ]);

        return Takmir::create([
            'mosque_id' => $takmir->mosque_id,
            'name' => $request['name'],
            'username' => $request['username'],
            'password' => bcrypt($request['password']),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Takmir  $takmir
     * @return \Illuminate\Http\Response
     */
    public function show(Takmir $takmir)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Takmir  $takmir
     * @return \Illuminate\Http\Response
     */
    public function edit(Takmir $takmir)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Takmir  $takmir
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Takmir $takmir)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'username' => 'required|string|max:15|unique:takmirs,username,NULL,mosque_id,mosque_id,'.$takmir->mosque_id.',id,id'.$takmir->id,
            'password' => 'nullable|string|max:255',
        ]);

        $takmir->name = $request->input('name');
        $takmir->username = $request->input('username');
        if ($request->input('password')) {
            $takmir->password = bcrypt($request->input('password'));
        }
        $takmir->save();
        return ['message' => 'Data updated!'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Takmir  $takmir
     * @return \Illuminate\Http\Response
     */
    public function destroy(Takmir $takmir)
    {
        $takmir->delete();
        return ['message' => 'Data Deleted!'];
    }

    public function login(Request $request)
    {
        if (!$request->input('mosque_number')) {
          return response()->json([
              'code' => 400,
              'message' => 'Field `mosque_number` required.'
          ], 400);
        }

        if (!$request->input('username')) {
          return response()->json([
              'code' => 400,
              'message' => 'Field `username` required.'
          ], 400);
        }

        if (!$request->input('password')) {
          return response()->json([
              'code' => 400,
              'message' => 'Field `password` required.'
          ], 400);
        }

        $mosque = Mosque::where('number', $request->input('mosque_number'))->first();
        if (!$mosque) {
          return response()->json([
            'code' => 404,
            'message' => 'User not found.'
          ], 404);
        }

        $takmir = Takmir::where('username', $request->input('username'))
                          ->where('mosque_id', $mosque->id)
                          ->first();
        if (!$takmir) {
          return response()->json([
              'code' => 404,
              'message' => 'User not found.'
          ], 404);
        }

        if (!Hash::check($request->input('password'), $takmir->password)) {
          return response()->json([
              'code' => 403,
              'message' => 'Password not match.'
          ], 403);
        }

        $token = $takmir->createToken('Access Token Takmir')->accessToken;
        $takmir->token = $token;

        $takmirToken = new TakmirToken;
        $takmirToken->takmir_id = $takmir->id;
        $takmirToken->token = $token;
        $takmirToken->save();

        return fractal()
               ->item($takmir, new TakmirTransformer())
               ->serializeWith(new ArraySerializer())
               ->toArray();
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        $token = $request->bearerToken();
        $takmirToken = TakmirToken::where('token', $token)->delete();

        return response()->json([
          'code' => 200,
          'message' => $request->bearerToken()
        ], 200);
    }

    public function check(Request $request)
    {
        $data = Auth::user();
        $data->token = $request->bearerToken();

        return fractal()
               ->item($data, new TakmirTransformer())
               ->serializeWith(new ArraySerializer())
               ->toArray();
    }

    public function test(Request $request)
    {
        dd(Hash::make('password'));
    }
}
