<?php

namespace App\Http\Controllers;

use App\Models\Device;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Mosque;
use App\Models\DeviceToken;
use Carbon\Carbon;
use App\Transformers\Serializer\ArraySerializer;
use App\Transformers\DeviceTransformer;
use App\Transformers\MosqueTransformer;
use App\Models\Configuration;
use App\Models\Solat, App\Models\Hijriah;
use App\Transformers\SolatTransformer;
use App\Transformers\HijriahTransformer;
use GuzzleHttp\Client, GuzzleHttp\Exception\ClientException, GuzzleHttp\Psr7;;

class DeviceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $takmir = Auth::user();
        $mosque_id = $takmir->mosque_id;

        if ($request->input('acp') == 'masjid') {
          $columns = ['', 'number', 'type', 'otp', 'otp_valid', 'status_confirmation', 'status'];

          $length = $request->input('length');
          $column = $request->input('column');
          $dir = $request->input('dir');
          $searchValue = $request->input('search');

          $query = Device::orderBy($columns[$column], $dir)
                           ->where('mosque_id', $mosque_id);

          if ($searchValue) {
            $query->where(function($query) use ($searchValue) {
              $query->where('number', 'like', '%' . $searchValue . '%')
                    ->orWhere('type', 'like', '%' . $searchValue . '%')
                    ->orWhere('otp', 'like', '%' . $searchValue . '%')
                    ->orWhere('otp_valid', 'like', '%' . $searchValue . '%')
                    ->orWhere('status_confirmation', 'like', '%' . $searchValue . '%')
                    ->orWhere('status', 'like', '%' . $searchValue . '%');
            });
          }

          $data = $query->paginate($length);
          return ['data' => $data, 'draw' => $request->input('draw')];
        } else {
          return [];
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Device  $device
     * @return \Illuminate\Http\Response
     */
    public function show(Device $device)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Device  $device
     * @return \Illuminate\Http\Response
     */
    public function edit(Device $device)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Device  $device
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Device $device)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'type' => 'required|integer|max:2',
        ]);

        $device->name = $request->input('name');
        $device->type = $request->input('type');
        $device->save();
        return ['message' => 'Data updated!'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Device  $device
     * @return \Illuminate\Http\Response
     */
    public function destroy(Device $device)
    {
        $device->delete();
        return ['message' => 'Data Deleted!'];
    }

    public function status(Request $request, Device $device)
    {
        $this->validate($request, [
            'status' => 'required|integer|max:2',
        ]);

        $device->status = $request->input('status');
        $device->save();
        return ['message' => 'Data updated!'];
    }

    public function register(Request $request)
    {
        if (!$request->input('id')) {
          return response()->json([
              'code' => 400,
              'message' => 'Field `id` required.'
          ], 400);
        }

        if (!$request->input('uuid')) {
          return response()->json([
              'code' => 400,
              'message' => 'Field `uuid` required.'
          ], 400);
        }

        $mosque = Mosque::where('number', $request->input('uuid'))->first();
        if (!$mosque) {
          return response()->json([
              'code' => 404,
              'message' => 'Mosque not found.'
          ], 404);
        }

        $mosque = fractal()
                  ->item($mosque, new MosqueTransformer())
                  ->serializeWith(new ArraySerializer())
                  ->toArray();

        $countDevice = Device::where('mosque_id', $mosque)->count();
        if ($mosque['package']['count_number'] != 0 && $countDevice >= $mosque['package']['count_number']) {
            return response()->json([
                'code' => 403,
                'message' => 'Device has reached the limit.'
            ], 403);
        }

        $device = Device::where('number', $request->input('id'))->first();
        if ($device) {

            if ($device->mosque_id != $mosque['id']) {
              return response()->json([
                  'code' => 403,
                  'message' => 'Device already used by another mosque.'
              ], 403);
            }

            $token = DeviceToken::where('device_id', $device->id)->first();
            if ($token) {

                $headers = [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer '.$token->token,
                ];
                $client = new \GuzzleHttp\Client([
                    'headers' => $headers
                ]);

                $response = $client->request('POST', env('APP_URL').'/api/v1/device/logout', []);
            }

            $expired = Carbon::now()->add('hour', 2)->isoFormat('YYYY-MM-DD H:mm:ss');
            $otp = rand(100000, 999999);

            $device->otp = $otp;
            $device->otp_valid = $expired;
            $device->save();

            $deviceNew = new Device;
            $deviceNew->mosque_id = $mosque['id'];
            $deviceNew->number = $request->input('id');
            $deviceNew->name = 'Device '.$request->input('id');

            return response()->json($deviceNew, 200);
        }

        $device = new Device;
        $device->mosque_id = $mosque['id'];
        $device->number = $request->input('id');
        $device->name = 'Device '.$request->input('id');
        $device->save();

        return response()->json($device, 200);
    }

    public function login(Request $request)
    {
        if (!$request->input('id')) {
          return response()->json([
              'code' => 400,
              'message' => 'Field `id` required.'
          ], 400);
        }

        if (!$request->input('otp')) {
          return response()->json([
              'code' => 400,
              'message' => 'Field `otp` required.'
          ], 400);
        }

        $device = Device::where('number', $request->input('id'))
                          ->where('status', 1)
                          ->first();

        if (!$device) {
          return response()->json([
              'code' => 404,
              'message' => 'Device not found.'
          ], 404);
        }

        if ($device->status_confirmation != 2) {
          return response()->json([
              'code' => 401,
              'message' => 'Device not yet approved.'
          ], 401);
        }

        if ($device->otp != $request->input('otp')) {
          return response()->json([
              'code' => 403,
              'message' => 'Invalid OTP.'
          ], 403);
        }

        $timenow = Carbon::now();
        $valid = Carbon::parse($device->otp_valid);

        if (!$valid->greaterThan($timenow)) {
          return response()->json([
              'code' => 403,
              'message' => 'OTP expired.'
          ], 403);
        }

        $expired = Carbon::now()->isoFormat('YYYY-MM-DD H:mm:ss');
        $device->otp_valid = $expired;
        $device->save();

        $token = DeviceToken::where('device_id', $device->id)->first();
        if ($token) {

            $headers = [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer '.$token->token,
            ];
            $client = new \GuzzleHttp\Client([
                'headers' => $headers
            ]);

            $response = $client->request('POST', env('APP_URL').'/api/v1/device/logout', []);
        }

        $token = $device->createToken('Access Token Device')->accessToken;
        $device->token = $token;

        $deviceToken = new DeviceToken;
        $deviceToken->device_id = $device->id;
        $deviceToken->token = $token;
        $deviceToken->save();

        return fractal()
               ->item($device, new DeviceTransformer())
               ->serializeWith(new ArraySerializer())
               ->toArray();
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        $request->user()->token()->delete();
        $deviceToken = DeviceToken::where('token', $request->bearerToken())->delete();

        return response()->json([
          'code' => 200,
          'message' => 'Success'
        ], 200);
    }

    public function check(Request $request)
    {
        $data = Auth::user();
        $data->token = $request->bearerToken();

        return fractal()
               ->item($data, new DeviceTransformer())
               ->serializeWith(new ArraySerializer())
               ->toArray();
    }

    public function verification(Request $request, Device $device)
    {
        if ($request->input('number') != $device->number) {
            return response()->json([
                'code' => 403,
                'message' => 'Device Id tidak sesuai.',
            ], 403);
        }

        $expired = Carbon::now()->add('hour', 2)->isoFormat('YYYY-MM-DD H:mm:ss');
        $otp = rand(100000, 999999);

        $device->type = $request->input('type');
        $device->otp = $otp;
        $device->otp_valid = $expired;
        $device->status_confirmation = 2;
        $device->save();

        return response()->json([
            'code' => 200,
            'message' => $device,
        ], 200);
    }

    public function otpRegenerate(Request $request, Device $device)
    {
        $timenow = Carbon::now();
        $valid = Carbon::parse($device->otp_valid);

        if ($valid->greaterThan($timenow)) {
          return response()->json([
              'code' => 403,
              'message' => 'Please wait until OTP expired.'
          ], 403);
        }

        $expired = Carbon::now()->add('hour', 2)->isoFormat('YYYY-MM-DD H:mm:ss');
        $otp = rand(100000, 999999);

        $device->otp = $otp;
        $device->otp_valid = $expired;
        $device->save();

        return response()->json([
            'code' => 200,
            'message' => $device,
        ], 200);
    }

    public function getData(Request $request)
    {
        $data = Auth::user();

        $device = Device::where('id', $data->id)->first();
        if ($device) {
            $device->last_sync = date('Y-m-d H:i:s');
            $device->save();
        }

        $device->token = $request->bearerToken();
        $mosque_id = $device->mosque_id;

        $response['device'] = fractal()
                              ->item($device, new DeviceTransformer())
                              ->serializeWith(new ArraySerializer())
                              ->toArray();

        $queryMosque = Mosque::where('id', $mosque_id)->first();
        $queryMosque->layout = $device->type;
        $response['mosque'] = fractal()
                              ->item($queryMosque, new MosqueTransformer())
                              ->serializeWith(new ArraySerializer())
                              ->parseIncludes(['runningtext', 'info', 'source', 'slideshow', 'solat'])
                              ->toArray();

        return response()->json($response, 200);
    }

    public function updateVersion(Request $request)
    {
        if (!$request->input('application_version')) {
            return response()->json([
                'code' => 400,
                'message' => 'Field `application_version` required.',
            ], 400);
        }

        $data = Auth::user();
        $device = Device::where('id', $data->id)->first();
        if (!$device) {
            return response()->json([
                'code' => 403,
                'message' => 'Device not found.',
            ], 403);
        }

        $device->application_version = $request->input('application_version');
        $device->save();

        $device->token = $request->bearerToken();
        // $version = Configuration::where('key', 'version')->first();

        // return response()->json([
        //     'code' => 200,
        //     'message' => $device
        // ], 200);

        return fractal()
               ->item($device, new DeviceTransformer())
               ->serializeWith(new ArraySerializer())
               ->toArray();
    }

    public function getDataAdhan(Request $request, $month = NULL, $year = NULL)
    {
        $data = Auth::user();

        if ($month) {
            if (strlen($month) < 1 || strlen($month) > 2 || $month > 12) {
                return response()->json([
                    'code' => 400,
                    'message' => 'Month not valid.'
                ], 400);
            }
            if (!$year) {
                return response()->json([
                    'code' => 400,
                    'message' => 'Year required if used month.'
                ], 400);
            }
            if (strlen($year) != 4) {
                return response()->json([
                    'code' => 400,
                    'message' => 'Year not valid.'
                ], 400);
            }
        }

        $device = Device::where('id', $data->id)->first();
        $mosque_id = $device->mosque_id;

        $query = Solat::where('mosque_id', $mosque_id);
        if ($month) {
            $query->whereMonth('dateTime', $month)->whereYear('dateTime', $year);
        }
        $query = $query->get();

        if ($query->count() == 0) {

            $client = new Client();
            $mosque = Mosque::where('id', $mosque_id)->first();

            if (!$month) {
                $month = date('m');
                $year = date('Y');
            }

            try {
                $response = $client->request('GET', env('API_ADHAN'), [
                    'query' => [
                        'latitude' => $mosque->latitude,
                        'longitude' => $mosque->longitude,
                        'month' => $month,
                        'year' => $year,
                        'method' => "99",
                        'methodSettings' => "19.5,1.5,18.5",
                        'tune' => "0,0,-2,3,2,0,0,0,39",
                    ]
                ]);

                $data = json_decode($response->getBody());

                foreach ($data->data as $result) {

                    $date = date('Y-m-d', strtotime($result->date->gregorian->year.'-'.$result->date->gregorian->month->number.'-'.$result->date->gregorian->day));

                    // CHECK HIJRIAH CALENDER
                    $checkHijriah = Hijriah::where('adDate', $date)->first();
                    if (!$checkHijriah) {
                        $hijriah = new Hijriah();
                        $hijriah->id = $result->date->hijri->date;
                        $hijriah->adDate = $date;
                        $hijriah->date = $result->date->hijri->day;
                        $hijriah->month = $result->date->hijri->month->number;
                        $hijriah->month_name = $result->date->hijri->month->en;
                        $hijriah->year = $result->date->hijri->year;
                        $hijriah->save();
                    }

                    foreach ($result->timings as $key => $value) {
                        $id = "";
                        $name = "";
                        $isAdhan = true;

                        if ($key == "Fajr") {
                            $id = "shubuh-";
                            $name = "Shubuh";
                        } else if ($key == "Dhuhr") {
                            $id = "dzuhur-";
                            $name = "Dzuhur";
                        } else if ($key == "Asr") {
                            $id = "ashar-";
                            $name = "Ashar";
                        } else if ($key == "Maghrib") {
                            $id = "magrib-";
                            $name = "Maghrib";
                        } else if ($key == "Isha") {
                            $id = "isya-";
                            $name = "Isha";
                        } else if ($key == "Imsak") {
                            $id = "imsak-";
                            $name = "Imsak";
                            $isAdhan = false;
                        } else if ($key == "Sunrise") {
                            $id = "terbit-";
                            $name = "Terbit";
                            $isAdhan = false;
                        } else {
                            continue;
                        }

                        $solat = new Solat();
                        $solat->id = $id . strtotime($date);
                        $solat->mosque_id = $mosque->id;
                        $solat->name = $name;
                        $solat->dateTime = $date . " " . substr($value, 0, 5);
                        $solat->isAdhan = $isAdhan;
                        $solat->save();
                    }
                }
            } catch (ClientException $e) {
                return response()->json([
                    'code' => 400,
                    'message' =>  Psr7\str($e->getResponse())
                ], 400);
            }

            if ($month) {
                $query = Hijriah::whereMonth('adDate', $month)->whereYear('adDate', $year)->get();
            } else {
                $query = Hijriah::get();
            }

            return fractal()
                   ->collection($query, new HijriahTransformer())
                   ->serializeWith(new ArraySerializer())
                   ->toArray();
        }

        return fractal()
               ->collection($query, new SolatTransformer())
               ->serializeWith(new ArraySerializer())
               ->toArray();
    }

    public function getDataHijriah(Request $request, $month = NULL, $year = NULL)
    {
        $data = Auth::user();

        if ($month) {
            if (strlen($month) < 1 || strlen($month) > 2 || $month > 12) {
                return response()->json([
                    'code' => 400,
                    'message' => 'Month not valid.'
                ], 400);
            }
            if (!$year) {
                return response()->json([
                    'code' => 400,
                    'message' => 'Year required if used month.'
                ], 400);
            }
            if (strlen($year) != 4) {
                return response()->json([
                    'code' => 400,
                    'message' => 'Year not valid.'
                ], 400);
            }
        }

        $device = Device::where('id', $data->id)->first();
        $mosque_id = $device->mosque_id;

        if ($month) {
            $query = Hijriah::whereMonth('adDate', $month)->whereYear('adDate', $year)->get();
        } else {
            $query = Hijriah::get();
        }

        if ($query->count() == 0) {

            $client = new Client();
            $mosque = Mosque::where('id', $mosque_id)->first();

            if (!$month) {
                $month = date('m');
                $year = date('Y');
            }

            try {
                $response = $client->request('GET', env('API_ADHAN'), [
                    'query' => [
                        'latitude' => $mosque->latitude,
                        'longitude' => $mosque->longitude,
                        'month' => $month,
                        'year' => $year,
                        'method' => "99",
                        'methodSettings' => "19.5,1.5,18.5",
                        'tune' => "0,0,-2,3,2,0,0,0,39",
                    ]
                ]);

                $data = json_decode($response->getBody());

                foreach ($data->data as $result) {

                    $date = date('Y-m-d', strtotime($result->date->gregorian->year.'-'.$result->date->gregorian->month->number.'-'.$result->date->gregorian->day));

                    // CHECK HIJRIAH CALENDER
                    $checkHijriah = Hijriah::where('adDate', $date)->first();
                    if (!$checkHijriah) {
                        $hijriah = new Hijriah();
                        $hijriah->id = $result->date->hijri->date;
                        $hijriah->adDate = $date;
                        $hijriah->date = $result->date->hijri->day;
                        $hijriah->month = $result->date->hijri->month->number;
                        $hijriah->month_name = $result->date->hijri->month->en;
                        $hijriah->year = $result->date->hijri->year;
                        $hijriah->save();
                    }

                    foreach ($result->timings as $key => $value) {
                        $id = "";
                        $name = "";
                        $isAdhan = true;

                        if ($key == "Fajr") {
                            $id = "shubuh-";
                            $name = "Shubuh";
                        } else if ($key == "Dhuhr") {
                            $id = "dzuhur-";
                            $name = "Dzuhur";
                        } else if ($key == "Asr") {
                            $id = "ashar-";
                            $name = "Ashar";
                        } else if ($key == "Maghrib") {
                            $id = "magrib-";
                            $name = "Maghrib";
                        } else if ($key == "Isha") {
                            $id = "isya-";
                            $name = "Isha";
                        } else if ($key == "Imsak") {
                            $id = "imsak-";
                            $name = "Imsak";
                            $isAdhan = false;
                        } else if ($key == "Sunrise") {
                            $id = "terbit-";
                            $name = "Terbit";
                            $isAdhan = false;
                        } else {
                            continue;
                        }

                        $solat = new Solat();
                        $solat->id = $id . strtotime($date);
                        $solat->mosque_id = $mosque->id;
                        $solat->name = $name;
                        $solat->dateTime = $date . " " . substr($value, 0, 5);
                        $solat->isAdhan = $isAdhan;
                        $solat->save();
                    }
                }
            } catch (ClientException $e) {
                return response()->json([
                    'code' => 400,
                    'message' =>  Psr7\str($e->getResponse())
                ], 400);
            }

            if ($month) {
                $query = Hijriah::whereMonth('adDate', $month)->whereYear('adDate', $year)->get();
            } else {
                $query = Hijriah::get();
            }

            return fractal()
                   ->collection($query, new HijriahTransformer())
                   ->serializeWith(new ArraySerializer())
                   ->toArray();
        }

        return fractal()
               ->collection($query, new HijriahTransformer())
               ->serializeWith(new ArraySerializer())
               ->toArray();
    }
}
