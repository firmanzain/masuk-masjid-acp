<?php

namespace App\Http\Controllers;

use App\Models\Preacher;
use Illuminate\Http\Request;
use App\Transformers\Serializer\ArraySerializer;
use App\Transformers\PreacherTransformer;
use Illuminate\Support\Facades\Auth;

class PreacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $takmir = Auth::user();
        $mosque_id = $takmir->mosque_id;

        if ($request->input('acp') == 'masjid') {
          $columns = ['', 'date', 'khotib', 'bilal', 'status'];

          $length = $request->input('length');
          $column = $request->input('column');
          $dir = $request->input('dir');
          $searchValue = $request->input('search');

          $query = Preacher::orderBy($columns[$column], $dir)
                             ->where('mosque_id', $mosque_id);

          if ($searchValue) {
            $query->where(function($query) use ($searchValue) {
              $query->where('date', 'like', '%' . $searchValue . '%')
                    ->orWhere('khotib', 'like', '%' . $searchValue . '%')
                    ->orWhere('bilal', 'like', '%' . $searchValue . '%')
                    ->orWhere('status', 'like', '%' . $searchValue . '%');
            });
          }

          $data = $query->paginate($length);
          return ['data' => $data, 'draw' => $request->input('draw')];
        } else {
          return [];
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $takmir = Auth::user();

        $this->validate($request, [
            'date' => 'required|date|max:255',
            'khotib' => 'required|string|max:255',
            'bilal' => 'required|string|max:255',
        ]);

        $alwaysShow = 0;
        if ($request['always_show']) {
          $alwaysShow = 1;
        }

        return Preacher::create([
            'mosque_id' => $takmir->mosque_id,
            'date' => $request['date'],
            'khotib' => $request['khotib'],
            'bilal' => $request['bilal'],
            'always_show' => $alwaysShow,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Preacher  $preacher
     * @return \Illuminate\Http\Response
     */
    public function show(Preacher $preacher)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Preacher  $preacher
     * @return \Illuminate\Http\Response
     */
    public function edit(Preacher $preacher)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Preacher  $preacher
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Preacher $preacher)
    {
        $this->validate($request, [
            'date' => 'required|date|max:255',
            'khotib' => 'required|string|max:255',
            'bilal' => 'required|string|max:255',
        ]);

        $preacher->date = $request->input('date');
        $preacher->khotib = $request->input('khotib');
        $preacher->bilal = $request->input('bilal');
        $preacher->always_show = 0;
        if ($request->input('always_show')) {
          $preacher->always_show = 1;
        }
        $preacher->save();
        return ['message' => 'Data updated!'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Preacher  $preacher
     * @return \Illuminate\Http\Response
     */
    public function destroy(Preacher $preacher)
    {
        $preacher->delete();
        return ['message' => 'Data Deleted!'];
    }

    public function status(Request $request, Preacher $preacher)
    {
        $this->validate($request, [
            'status' => 'required|integer|max:2',
        ]);

        $preacher->status = $request->input('status');
        $preacher->save();
        return ['message' => 'Data updated!'];
    }
}
