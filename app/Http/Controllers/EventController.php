<?php

namespace App\Http\Controllers;

use App\Models\Event, App\Models\EventItem;
use Illuminate\Http\Request;
use App\Transformers\Serializer\ArraySerializer;
use App\Transformers\EventTransformer;
use Illuminate\Support\Facades\Auth;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $takmir = Auth::user();
        $mosque_id = $takmir->mosque_id;

        if ($request->input('acp') == 'masjid') {
          $columns = ['', 'date', 'show_at', 'title', 'status'];

          $length = $request->input('length');
          $column = $request->input('column');
          $dir = $request->input('dir');
          $searchValue = $request->input('search');

          $query = Event::with('items')
                          ->orderBy($columns[$column], $dir)
                          ->where('mosque_id', $mosque_id);

          if ($searchValue) {
            $query->where(function($query) use ($searchValue) {
              $query->where('date', 'like', '%' . $searchValue . '%')
                    ->orWhere('show_at', 'like', '%' . $searchValue . '%')
                    ->orWhere('title', 'like', '%' . $searchValue . '%')
                    ->orWhere('status', 'like', '%' . $searchValue . '%');
            });
          }

          $data = $query->paginate($length);
          return ['data' => $data, 'draw' => $request->input('draw')];
        } else {
          return [];
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $takmir = Auth::user();

        $this->validate($request, [
            'date' => 'required|date|max:255',
            'show_at' => 'required|date|max:255',
            'title' => 'required|string|max:30',
        ]);

        $alwaysShow = 0;
        if ($request['always_show']) {
          $alwaysShow = 1;
        }

        $event = Event::create([
            'mosque_id' => $takmir->mosque_id,
            'date' => $request['date'],
            'show_at' => $request['show_at'],
            'title' => $request['title'],
            'always_show' => $alwaysShow,
        ]);

        foreach ($request['items'] as $item) {
            $eventItem = EventItem::create([
                'event_id' => $event->id,
                'key' => $item['key'],
                'value' => $item['value'],
            ]);
        }

        return $event;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Event $event)
    {
        $this->validate($request, [
            'date' => 'required|date|max:255',
            'show_at' => 'required|date|max:255',
            'title' => 'required|string|max:20',
        ]);

        $event->date = $request->input('date');
        $event->show_at = $request->input('show_at');
        $event->title = $request->input('title');
        $event->always_show = 0;
        if ($request->input('always_show')) {
          $event->always_show = 1;
        }
        $event->save();

        EventItem::where('event_id', $event->id)->delete();
        foreach ($request['items'] as $item) {
            $eventItem = EventItem::create([
                'event_id' => $event->id,
                'key' => $item['key'],
                'value' => $item['value'],
            ]);
        }

        return ['message' => 'Data updated!'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event)
    {
        $event->delete();
        EventItem::where('event_id', $event->id)->delete();

        return ['message' => 'Data Deleted!'];
    }

    public function status(Request $request, Event $event)
    {
        $this->validate($request, [
            'status' => 'required|integer|max:2',
        ]);

        $event->status = $request->input('status');
        $event->save();
        return ['message' => 'Data updated!'];
    }
}
