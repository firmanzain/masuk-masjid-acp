<?php

namespace App\Http\Controllers;

use App\Models\Mosque;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use App\Models\Province, App\Models\Takmir;
use Carbon\Carbon;
use App\Models\PrayerDuration;
use GuzzleHttp\Client, GuzzleHttp\Exception\ClientException;
use App\Models\Hijriah, App\Models\Solat, App\Models\TextArabic,
App\Models\ImageBackground, App\Models\Package, App\Models\Team,
App\Models\TeamBalance;

class MosqueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['mosque'] = Mosque::with(['province'])->get();

        return view('mosque', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = array();

        return view('mosque-form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $urut = Mosque::where('province_id', $request->post('province'))
                        ->orderBy('id', 'DESC')->first();
        if ($urut) {
            $urut = substr($urut->number, 2, 6);
            $urut = intval($urut);
            $urut++;
        } else {
            $urut = 1;
        }
        $seq = str_pad($urut, 6, '0', STR_PAD_LEFT);

        $provincecode = '00';
        $province = Province::where('id', $request->post('province'))->first();
        if ($province) {
            $provincecode = $province->code;
        }

        // $date = Carbon::parse($request->post('last_subscribe'));
        $package = Package::where('id', $request->post('package_id'))->first();

        $date = date('Y-m-d');
        $monthSub = intval(date('m', strtotime($date)));
        $yearSub = intval(date('Y', strtotime($date)));
        $monthSub += ($package->month - 1);

        if ($monthSub > 12) {
          $yearSub += intval($monthSub/12);
          $monthSub = intval(fmod($monthSub, 12));
        }

        $last_subscribe = date('Y-m-t', strtotime($yearSub.'-'.$monthSub.'-01'));

        $mosque = new Mosque;
        $mosque->number = $provincecode.$seq;
        $mosque->name = $request->post('name');
        $mosque->logo = env('APP_URL').'/storage/images/logo/default.png';
        $mosque->province_id = $request->post('province');
        $mosque->address = $request->post('address');
        $mosque->latitude = 0;
        $mosque->longitude = 0;
        // $mosque->last_subscribe = $date->isoFormat('Y-MM-DD');
        $mosque->last_subscribe = $last_subscribe;
        $mosque->package_id = $request->post('package_id');
        $mosque->status = -4;
        $mosque->show_information = '["name","logo","address"]';
        $mosque->team_id = $request->post('team_id');
        $mosque->save();

        $takmir = new Takmir;
        $takmir->mosque_id = $mosque->id;
        $takmir->name = $request->post('takmir_name');
        $takmir->username = $request->post('username');
        $takmir->password = bcrypt($request->post('password'));
        $takmir->save();

        $solat = array('subuh', 'zuhur', 'asar', 'magrib', 'isya', 'jumat');
        for ($i = 0; $i < sizeof($solat); $i++) {
            $duration = new PrayerDuration;
            $duration->mosque_id = $mosque->id;
            $duration->name = $solat[$i];
            if ($solat[$i] != 'jumat') {
              $duration->azan_duration = '00:03:00';
              $duration->iqamat_duration = '00:05:00';
              $duration->solat_duration = '00:10:00';
            } else {
              $duration->start_at = '12:00:00';
              $duration->solat_duration = '00:37:00';
            }
            $duration->save();
        }

        // Arabic
        $defaultArabic = array(
            array(
                "arabic" => "صَلاَةُ الْجَمَاعَةِ أَفْضَلُ مِنْ صَلاَةِ الْفَذِّ بِسَبْعٍ وَعِشْرِينَ دَرَجَةً",
                "translate" => "Shalat jamaah lebih baik 27 derajat dibanding shalat sendirian.",
                "origin" => "HR. Bukhari, no. 645 dan Muslim, no. 650"
            ),
            array(
                "arabic" => "وَلَوْ يَعْلَمُونَ مَا فِي العَتَمَةِ وَالصُّبْحِ، لَأَتَوْهُمَا وَلَ",
                "translate" => "Seandainya orang-orang mengetahui keutamaan yang terdapat dalam shalat isya dan subuh, niscaya mereka akan mendatanginya meskipun dengan merangkak.",
                "origin" => "HR. Muttafaq ‘alaih"
            ),
            array(
                "arabic" => "حَافِظُوا عَلَى الصَّلَوَاتِ وَالصَّلَاةِ الْوُسْطَىٰ وَقُومُوا لِلَّهِ قَانِتِينَ",
                "translate" => "Peliharalah semua shalat(mu), dan (peliharalah) shalat wusthaa. Berdirilah untuk Allah (dalam shalatmu) dengan khusyu´.",
                "origin" => "QS. Al-Baqarah : 238"
            ),
            array(
                "arabic" => "فَإِنْ تَابُوا وَأَقَامُوا الصَّلَاةَ وَآتَوُا الزَّكَاةَ فَخَلُّوا سَبِيلَهُمْ",
                "translate" => "Jika mereka bertaubat dan mendirikan sholat dan menunaikan zakat, maka berilah kebebasan kepada mereka untuk berjalan.",
                "origin" => "QS. At-Taubah : 5"
            ),
        );

        $selected = array();
        $textArabic = array();
        for ($i = 0; $i < 3; $i++) {
            $flag = 0;
            if (sizeof($selected) != 0) {
                $flag = 0;
                do {
                    $rand = rand(1, 4);
                    if (in_array($rand, $selected)) {
                    } else {
                        $flag = 1;
                        array_push($selected, $rand);
                        array_push($textArabic, $defaultArabic[$rand-1]);
                    }
                } while ($flag == 0);
            } else {
                $rand = rand(1, 4);
                array_push($selected, $rand);
                array_push($textArabic, $defaultArabic[$rand-1]);
            }
        }

        for ($i = 0; $i < sizeof($textArabic); $i++) {
            TextArabic::create([
                'mosque_id' => $mosque->id,
                'arabic' => $textArabic[$i]['arabic'],
                'translate' => $textArabic[$i]['translate'],
                'origin' => $textArabic[$i]['origin'],
                'location' => 2,
                'start_at' => date('Y-m-d'),
                'end_at' => date('Y-m-t'),
                'always_show' => 1,
            ]);
        }
        // End Arabic

        // Image Background
        $selected = array();
        $imageBackground = array();
        for ($i = 0; $i < 3; $i++) {
            $flag = 0;
            if (sizeof($selected) != 0) {
                $flag = 0;
                do {
                    $rand = rand(1, 11);
                    if (in_array($rand, $selected)) {
                    } else {
                        $flag = 1;
                        array_push($selected, $rand);
                        array_push($imageBackground, env('APP_URL').'/storage/images/background/default/default'.$rand.'.jpeg');
                    }
                } while ($flag == 0);
            } else {
                $rand = rand(1, 11);
                array_push($selected, $rand);
                array_push($imageBackground, env('APP_URL').'/storage/images/background/default/default'.$rand.'.jpeg');
            }
        }

        for ($i = 0; $i < sizeof($imageBackground); $i++) {
            ImageBackground::create([
                'mosque_id' => $mosque->id,
                'name' => 'Default Clock '.($i+1),
                'image' => $imageBackground[$i],
                'information' => "-",
                'start_at' => date('Y-m-d'),
                'end_at' => date('Y-m-t'),
                'layout' => 1,
                'always_show' => 1,
                'view_layout' => '["slide","full"]',
            ]);
            ImageBackground::create([
                'mosque_id' => $mosque->id,
                'name' => 'Default Informasi '.($i+1),
                'image' => $imageBackground[$i],
                'information' => "-",
                'start_at' => date('Y-m-d'),
                'end_at' => date('Y-m-t'),
                'layout' => 2,
                'always_show' => 1,
                'view_layout' => '["slide","full"]',
            ]);
        }
        // End Image Background

        if ($request->post('team_id') != 0) {
            $team = Team::where('id', $request->post('team_id'))->first();
            if ($team) {
                $package = Package::where('id', $request->post('package_id'))->first();
                $nominal = 0;
                if ($package) {
                    $nominal = $package->price;
                }

                $balance = new TeamBalance();
                $balance->team_id = $team->id;
                $balance->date = date('Y-m-d');
                $balance->nominal = 20 / 100 * $nominal;
                $balance->information = 'Pendaftaran Masjid Baru ('.$request->post('name').')';
                $balance->save();
            }
        }

        return redirect('mosque')->with([
            'alert' => 'alert-success',
            'message' => 'Data has been saved.'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Mosque  $mosque
     * @return \Illuminate\Http\Response
     */
    public function show(Mosque $mosque)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Mosque  $mosque
     * @return \Illuminate\Http\Response
     */
    public function edit(Mosque $mosque)
    {
        $data['mosque'] = Mosque::with(['province', 'takmir'])->find($mosque->id);

        return view('mosque-form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Mosque  $mosque
     * @return \Illuminate\Http\Response
     */
    public function updateacp(Request $request, Mosque $mosque)
    {

        // $date = Carbon::parse($request->post('last_subscribe'));

        $mosque->name = $request->post('name');
        $mosque->province_id = $request->post('province');
        $mosque->address = $request->post('address');
        // $mosque->last_subscribe = $date->isoFormat('Y-MM-DD');
        $mosque->package_id = $request->post('package_id');
        $mosque->team_id = $request->post('team_id');
        $mosque->save();

        $takmir = Takmir::find($request->post('takmir_id'));
        if ($takmir) {
            $takmir->name = $request->post('takmir_name');
            if ($request->post('password')) {
                $takmir->password = bcrypt($request->post('password'));
            }
            $takmir->save();
        }

        return redirect('mosque')->with([
            'alert' => 'alert-success',
            'message' => 'Data has been saved.'
        ]);
    }

    public function update(Request $request, Mosque $mosque)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'logo' => 'nullable|image|max:1024',
            'province_id' => 'required|integer',
            'address' => 'required|string',
            'latitude' => 'required|string|max:255',
            'longitude' => 'required|string|max:255',
        ]);

        if ($request->hasFile('logo')) {
            $path = $request->file('logo')->storeAs('public/images/logo', Str::random(10).time().'.'.$request->file('logo')->getClientOriginalExtension());
            $mosque->logo = env('APP_URL').'/storage/'.str_replace('public/', '', $path);
        }
        $mosque->name = $request->input('name');
        $mosque->province_id = $request->input('province_id');
        $mosque->address = $request->input('address');
        $mosque->latitude = $request->post('latitude');
        $mosque->longitude = $request->post('longitude');
        $mosque->show_ads = 0;
        if ($request->post('show_ads')) {
          $mosque->show_ads = 1;
        }
        $show_information = array();
        if ($request->post('show_name')) {
            array_push($show_information, "name");
        }
        if ($request->post('show_logo')) {
            array_push($show_information, "logo");
        }
        if ($request->post('show_address')) {
            array_push($show_information, "address");
        }
        $mosque->show_information = json_encode($show_information);
        if ($mosque->status == -3) {
            $mosque->status = -2;
        } else if ($mosque->status == -4) {
            $mosque->status = -3;
        }
        $mosque->save();

        if ($mosque->status == -3) {
            $client = new Client();
            $date = date('Y-m-01');

            for ($i = 1; $i < 4; $i++) {

                if ($i != 1) {
                  $date = date('Y-m-01', strtotime($date . ' +1 month'));
                // } else {
                //   // DELETE OLD RECORDS
                //   $deleteHijriah = Hijriah::where('adDate', '<', $date)->delete();
                //   $deleteSolat = Solat::whereDate('dateTime', '<', $date)->delete();
                }

                $check = Solat::where('mosque_id', $mosque->id)
                                ->whereMonth('dateTime', date('m', strtotime($date)))
                                ->whereYear('dateTime', date('Y', strtotime($date)))
                                ->first();
                if ($check) {
                    continue;
                }

                try {
                    $response = $client->request('GET', env('API_ADHAN'), [
                        'query' => [
                          'latitude' => $mosque->latitude,
                          'longitude' => $mosque->longitude,
                          'month' => date('m', strtotime($date)),
                          'year' => date('Y', strtotime($date)),
                          'method' => "99",
                          'methodSettings' => "19.5,1.5,18.5",
                          'tune' => "0,0,-2,3,2,0,0,0,39",
                        ]
                    ]);

                    $data = json_decode($response->getBody());

                    foreach ($data->data as $result) {

                        $date = date('Y-m-d', strtotime($result->date->gregorian->year.'-'.$result->date->gregorian->month->number.'-'.$result->date->gregorian->day));

                        // CHECK HIJRIAH CALENDER
                        $checkHijriah = Hijriah::where('adDate', $date)->first();
                        if (!$checkHijriah) {
                            $hijriah = new Hijriah();
                            $hijriah->id = $result->date->hijri->date;
                            $hijriah->adDate = $date;
                            $hijriah->date = $result->date->hijri->day;
                            $hijriah->month = $result->date->hijri->month->number;
                            $hijriah->month_name = $result->date->hijri->month->en;
                            $hijriah->year = $result->date->hijri->year;
                            $hijriah->save();
                        }

                        foreach ($result->timings as $key => $value) {
                            $id = "";
                            $name = "";
                            $isAdhan = true;

                            if ($key == "Fajr") {
                                $id = "shubuh-";
                                $name = "Shubuh";
                            } else if ($key == "Dhuhr") {
                                $id = "dzuhur-";
                                $name = "Dzuhur";
                            } else if ($key == "Asr") {
                                $id = "ashar-";
                                $name = "Ashar";
                            } else if ($key == "Maghrib") {
                                $id = "magrib-";
                                $name = "Maghrib";
                            } else if ($key == "Isha") {
                                $id = "isya-";
                                $name = "Isha";
                            } else if ($key == "Imsak") {
                                $id = "imsak-";
                                $name = "Imsak";
                                $isAdhan = false;
                            } else if ($key == "Sunrise") {
                                $id = "terbit-";
                                $name = "Terbit";
                                $isAdhan = false;
                            } else {
                                continue;
                            }

                            $solat = new Solat();
                            $solat->id = $id . strtotime($date);
                            $solat->mosque_id = $mosque->id;
                            $solat->name = $name;
                            $solat->dateTime = $date . " " . substr($value, 0, 5);
                            $solat->isAdhan = $isAdhan;
                            $solat->save();
                        }
                    }
                } catch (ClientException $e) {
                    continue;
                }
            }
        }

        return response()->json([
            'code' => 200,
            'message' => $mosque
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Mosque  $mosque
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mosque $mosque)
    {
        $mosque->delete();
        return response()->json([
            'code' => 200,
        ], 200);
    }

    public function updateStatus(Request $request, Mosque $mosque)
    {
        if ($mosque->status == -2) {
            $mosque->status = -1;
        } else if ($mosque->status == -1) {
            $mosque->status = 1;
        } else if ($mosque->status == -3) {
            $mosque->status = -2;
        }
        $mosque->save();

        return response()->json([
            'code' => 200,
            'message' => $mosque
        ], 200);
    }

    public function getAladhan(Request $request)
    {
        $client = new Client();
        $mosques = Mosque::get();
        $date = date('Y-m-01');

        if ($mosques) {
            foreach ($mosques as $mosque) {
                $date = date('Y-m-01');

                for ($i = 1; $i < 4; $i++) {

                    if ($i != 1) {
                      $date = date('Y-m-01', strtotime($date . ' +1 month'));
                    } else {
                      // DELETE OLD RECORDS
                      $deleteHijriah = Hijriah::where('adDate', '<', $date)->delete();
                      $deleteSolat = Solat::whereDate('dateTime', '<', $date)->delete();
                    }

                    $check = Solat::where('mosque_id', $mosque->id)
                                    ->whereMonth('dateTime', date('m', strtotime($date)))
                                    ->whereYear('dateTime', date('Y', strtotime($date)))
                                    ->first();
                    if ($check) {
                        continue;
                    }

                    try {
                        $response = $client->request('GET', env('API_ADHAN'), [
                            'query' => [
                              'latitude' => $mosque->latitude,
                              'longitude' => $mosque->longitude,
                              'month' => date('m', strtotime($date)),
                              'year' => date('Y', strtotime($date)),
                              'method' => "99",
                              'methodSettings' => "19.5,1.5,18.5",
                              'tune' => "0,0,-2,3,2,0,0,0,39",
                            ]
                        ]);

                        $data = json_decode($response->getBody());

                        foreach ($data->data as $result) {

                            $date = date('Y-m-d', strtotime($result->date->gregorian->year.'-'.$result->date->gregorian->month->number.'-'.$result->date->gregorian->day));

                            // CHECK HIJRIAH CALENDER
                            $checkHijriah = Hijriah::where('adDate', $date)->first();
                            if (!$checkHijriah) {
                                $hijriah = new Hijriah();
                                $hijriah->id = $result->date->hijri->date;
                                $hijriah->adDate = $date;
                                $hijriah->date = $result->date->hijri->day;
                                $hijriah->month = $result->date->hijri->month->number;
                                $hijriah->month_name = $result->date->hijri->month->en;
                                $hijriah->year = $result->date->hijri->year;
                                $hijriah->save();
                            }

                            foreach ($result->timings as $key => $value) {
                                $id = "";
                                $name = "";
                                $isAdhan = true;

                                if ($key == "Fajr") {
                                    $id = "shubuh-";
                                    $name = "Shubuh";
                                } else if ($key == "Dhuhr") {
                                    $id = "dzuhur-";
                                    $name = "Dzuhur";
                                } else if ($key == "Asr") {
                                    $id = "ashar-";
                                    $name = "Ashar";
                                } else if ($key == "Maghrib") {
                                    $id = "magrib-";
                                    $name = "Maghrib";
                                } else if ($key == "Isha") {
                                    $id = "isya-";
                                    $name = "Isha";
                                } else if ($key == "Imsak") {
                                    $id = "imsak-";
                                    $name = "Imsak";
                                    $isAdhan = false;
                                } else if ($key == "Sunrise") {
                                    $id = "terbit-";
                                    $name = "Terbit";
                                    $isAdhan = false;
                                } else {
                                    continue;
                                }

                                $solat = new Solat();
                                $solat->id = $id . strtotime($date);
                                $solat->mosque_id = $mosque->id;
                                $solat->name = $name;
                                $solat->dateTime = $date . " " . substr($value, 0, 5);
                                $solat->isAdhan = $isAdhan;
                                $solat->save();
                            }
                        }
                    } catch (ClientException $e) {
                        continue;
                    }
                }

            }
        }
    }

    public function test(Request $request)
    {
        $defaultArabic = array(
            array(
                "arabic" => "صَلاَةُ الْجَمَاعَةِ أَفْضَلُ مِنْ صَلاَةِ الْفَذِّ بِسَبْعٍ وَعِشْرِينَ دَرَجَةً",
                "translate" => "Shalat jamaah lebih baik 27 derajat dibanding shalat sendirian.",
                "origin" => "HR. Bukhari, no. 645 dan Muslim, no. 650"
            ),
            array(
                "arabic" => "وَلَوْ يَعْلَمُونَ مَا فِي العَتَمَةِ وَالصُّبْحِ، لَأَتَوْهُمَا وَلَ",
                "translate" => "Seandainya orang-orang mengetahui keutamaan yang terdapat dalam shalat isya dan subuh, niscaya mereka akan mendatanginya meskipun dengan merangkak.",
                "origin" => "HR. Muttafaq ‘alaih"
            ),
            array(
                "arabic" => "حَافِظُوا عَلَى الصَّلَوَاتِ وَالصَّلَاةِ الْوُسْطَىٰ وَقُومُوا لِلَّهِ قَانِتِينَ",
                "translate" => "Peliharalah semua shalat(mu), dan (peliharalah) shalat wusthaa. Berdirilah untuk Allah (dalam shalatmu) dengan khusyu´.",
                "origin" => "QS. Al-Baqarah : 238"
            ),
            array(
                "arabic" => "فَإِنْ تَابُوا وَأَقَامُوا الصَّلَاةَ وَآتَوُا الزَّكَاةَ فَخَلُّوا سَبِيلَهُمْ",
                "translate" => "Jika mereka bertaubat dan mendirikan sholat dan menunaikan zakat, maka berilah kebebasan kepada mereka untuk berjalan.",
                "origin" => "QS. At-Taubah : 5"
            ),
        );

        $selected = array();
        $textArabic = array();
        for ($i = 0; $i < 3; $i++) {
            $flag = 0;
            if (sizeof($selected) != 0) {
                $flag = 0;
                do {
                    $rand = rand(1, 4);
                    if (in_array($rand, $selected)) {
                    } else {
                        $flag = 1;
                        array_push($selected, $rand);
                        array_push($textArabic, $defaultArabic[$rand-1]);
                    }
                } while ($flag == 0);
            } else {
                $rand = rand(1, 4);
                array_push($selected, $rand);
                array_push($textArabic, $defaultArabic[$rand-1]);
            }
        }

        $selected = array();
        $imageBackground = array();
        for ($i = 0; $i < 3; $i++) {
            $flag = 0;
            if (sizeof($selected) != 0) {
                $flag = 0;
                do {
                    $rand = rand(1, 11);
                    if (in_array($rand, $selected)) {
                    } else {
                        $flag = 1;
                        array_push($selected, $rand);
                        array_push($imageBackground, env('APP_URL').'/storage/images/background/default/default'.$rand.'.jpeg');
                    }
                } while ($flag == 0);
            } else {
                $rand = rand(1, 11);
                array_push($selected, $rand);
                array_push($imageBackground, env('APP_URL').'/storage/images/background/default/default'.$rand.'.jpeg');
            }
        }

        dd($imageBackground);
    }

    public function renewsubscribe(Request $request, Mosque $mosque)
    {
        $package = Package::where('id', $mosque->package_id)->first();
        if ($package) {
            $monthSub = intval(date('m', strtotime($mosque->last_subscribe)));
            $yearSub = intval(date('Y', strtotime($mosque->last_subscribe)));
            $monthSub += $package->month;

            if ($monthSub > 12) {
                $yearSub += intval($monthSub/12);
                $monthSub = intval(fmod($monthSub, 12));
            }

            // $mosque->last_subscribe = date('Y-m-t', strtotime($yearSub.'-'.$monthSub.'-01'));

            $mosque->last_subscribe = date('Y-m-d', strtotime('+'.$package->month.' month', strtotime($mosque->last_subscribe)));
            $mosque->save();
        }

        return response()->json([
            'code' => 200,
            'message' => $mosque
        ], 200);
    }
}
