<?php

namespace App\Http\Controllers;

use App\Models\TeamWithdraw;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Team, App\Models\TeamBalance, App\Models\BankAccount;
use Illuminate\Support\Facades\DB;

class TeamWithdrawController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['withdraw'] = TeamWithdraw::with(['team'])->orderBy('date', 'DESC')->get();

        return view('withdraw', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $team = Auth::user();
        if (!$team) {
            return response()->json([
                'code' => 403,
                'message' => 'User not login.'
            ], 403);
        }

        $bankAccount = BankAccount::where('team_id', $team->id)->first();
        if (!$bankAccount) {
            return response()->json([
                'code' => 403,
                'message' => 'Akun Bank belum diset.'
            ], 403);
        }

        // $date = date('d');
        // if (intval($date) > 5) {
        //     return response()->json([
        //         'code' => 400,
        //         'message' => 'Penarikan hanya dapat dilakukan pada tanggal 1-5 setiap bulan.'
        //     ], 400);
        // }

        $withdrawNominal = intval(str_replace(',', '', $request->input('withdraw')));
        if ($withdrawNominal < 100000) {
            return response()->json([
                'code' => 400,
                'message' => 'Minimal penarikan sebesar Rp. 100.000.'
            ], 400);
        }
        $withdrawNominal += 5000;

        $balance = TeamBalance::where('team_id', $team->id)->sum('nominal');
        $withdraw = TeamWithdraw::select(DB::raw('SUM(nominal + admin) as nominal'))
                                  ->where('team_id', $team->id)->where('status', '!=', 0)
                                  ->first();

        $nominal = $balance - $withdraw->nominal;
        if ($withdrawNominal > $nominal) {
            return response()->json([
                'code' => 403,
                'message' => 'Saldo tidak mencukupi.'
            ], 403);
        }

        $data = new TeamWithdraw();
        $data->date = date('Y-m-d');
        $data->team_id = $team->id;
        $data->bank_account_id = $bankAccount->id;
        $data->nominal = $withdrawNominal;
        $data->admin = 5000;
        $data->information = 'Penarikan ke '.$request->input('bank_id_name').' No. Rekening '.$request->input('bank_number').' a/n '.$request->input('bank_name');
        $data->status = 2;
        $data->save();

        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TeamWithdraw  $teamWithdraw
     * @return \Illuminate\Http\Response
     */
    public function show(TeamWithdraw $teamWithdraw)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TeamWithdraw  $teamWithdraw
     * @return \Illuminate\Http\Response
     */
    public function edit(TeamWithdraw $teamWithdraw)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TeamWithdraw  $teamWithdraw
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TeamWithdraw $teamWithdraw)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TeamWithdraw  $teamWithdraw
     * @return \Illuminate\Http\Response
     */
    public function destroy(TeamWithdraw $teamWithdraw)
    {
        $teamWithdraw->delete();
        return response()->json([
            'code' => 200,
        ], 200);
    }

    public function accept(Request $request, $id)
    {
        $data = TeamWithdraw::find($id);
        if (!$data) {
            return response()->json([
                'code' => 404,
            ], 404);
        }

        $data->status = 1;
        $data->save();
        return response()->json([
            'code' => 200,
        ], 200);
    }

    public function decline(Request $request, $id)
    {
        $data = TeamWithdraw::find($id);
        if (!$data) {
            return response()->json([
                'code' => 404,
            ], 404);
        }

        $data->status = 0;
        $data->save();
        return response()->json([
            'code' => 200,
        ], 200);
    }
}
