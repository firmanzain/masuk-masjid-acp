<?php

namespace App\Http\Controllers;

use App\Models\Bank;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Mosque;
use App\Transformers\Serializer\ArraySerializer;
use App\Transformers\BankTransformer;

class BankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['bank'] = Bank::get();

        return view('bank', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = array();

        return view('bank-form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $bank = new Bank;
        $bank->name = $request->post('name');
        $bank->save();

        return redirect('bank')->with([
            'alert' => 'alert-success',
            'message' => 'Data has been saved.'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Bank  $bank
     * @return \Illuminate\Http\Response
     */
    public function show(Bank $bank)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Bank  $bank
     * @return \Illuminate\Http\Response
     */
    public function edit(Bank $bank)
    {
        $data['bank'] = Bank::find($bank->id);

        return view('bank-form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Bank  $bank
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Bank $bank)
    {

        $bank->name = $request->post('name');
        $bank->save();

        return redirect('bank')->with([
            'alert' => 'alert-success',
            'message' => 'Data has been saved.'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Bank  $bank
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bank $bank)
    {
        $bank->delete();
        return response()->json([
            'code' => 200,
        ], 200);
    }

    public function select(Request $request)
    {
        $bank = Bank::orderBy('name', 'ASC')->get();

        return fractal()
               ->collection($bank, new BankTransformer())
               ->serializeWith(new ArraySerializer())
               ->toArray();
    }
}
