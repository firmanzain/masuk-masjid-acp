<?php

namespace App\Http\Controllers;

use App\Models\Solat;
use Illuminate\Http\Request;

class SolatCotroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Solat  $solat
     * @return \Illuminate\Http\Response
     */
    public function show(Solat $solat)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Solat  $solat
     * @return \Illuminate\Http\Response
     */
    public function edit(Solat $solat)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Solat  $solat
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Solat $solat)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Solat  $solat
     * @return \Illuminate\Http\Response
     */
    public function destroy(Solat $solat)
    {
        //
    }
}
