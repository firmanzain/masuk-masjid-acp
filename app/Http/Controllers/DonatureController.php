<?php

namespace App\Http\Controllers;

use App\Models\Donature;
use Illuminate\Http\Request;
use App\Transformers\Serializer\ArraySerializer;
use App\Transformers\DonatureTransformer;
use Illuminate\Support\Facades\Auth;

class DonatureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $takmir = Auth::user();
        $mosque_id = $takmir->mosque_id;

        if ($request->input('acp') == 'masjid') {
          $columns = ['', 'date', 'name', 'address', 'amount', 'status'];

          $length = $request->input('length');
          $column = $request->input('column');
          $dir = $request->input('dir');
          $searchValue = $request->input('search');

          $query = Donature::orderBy($columns[$column], $dir)
                             ->where('mosque_id', $mosque_id);

          if ($searchValue) {
            $query->where(function($query) use ($searchValue) {
              $query->where('date', 'like', '%' . $searchValue . '%')
                    ->orWhere('name', 'like', '%' . $searchValue . '%')
                    ->orWhere('address', 'like', '%' . $searchValue . '%')
                    ->orWhere('amount', 'like', '%' . $searchValue . '%')
                    ->orWhere('status', 'like', '%' . $searchValue . '%');
            });
          }

          $data = $query->paginate($length);
          return ['data' => $data, 'draw' => $request->input('draw')];
        } else {
          return [];
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $takmir = Auth::user();

        $this->validate($request, [
            'date' => 'required|date|max:255',
            'name' => 'required|string|max:255',
            // 'address' => 'string',
            'amount' => 'required|string|max:255',
            // 'information' => 'string',
        ]);

        $alwaysShow = 0;
        if ($request['always_show']) {
          $alwaysShow = 1;
        }

        return Donature::create([
            'mosque_id' => $takmir->mosque_id,
            'date' => $request['date'],
            'name' => $request['name'],
            'address' => $request['address'],
            'amount' => str_replace(',', '', $request['amount']),
            'information' => $request['information'],
            'always_show' => $alwaysShow,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Donature  $donature
     * @return \Illuminate\Http\Response
     */
    public function show(Donature $donature)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Donature  $donature
     * @return \Illuminate\Http\Response
     */
    public function edit(Donature $donature)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Donature  $donature
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Donature $donature)
    {
        $this->validate($request, [
            'date' => 'required|date|max:255',
            'name' => 'required|string|max:255',
            // 'address' => 'string',
            'amount' => 'required|string|max:255',
            // 'information' => 'string',
        ]);

        $donature->date = $request->input('date');
        $donature->name = $request->input('name');
        $donature->address = $request->input('address');
        $donature->amount = str_replace(',', '', $request->input('amount'));
        $donature->information = $request->input('information');
        $donature->always_show = 0;
        if ($request->input('always_show')) {
          $donature->always_show = 1;
        }
        $donature->save();
        return ['message' => 'Data updated!'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Donature  $donature
     * @return \Illuminate\Http\Response
     */
    public function destroy(Donature $donature)
    {
        $donature->delete();
        return ['message' => 'Data Deleted!'];
    }

    public function status(Request $request, Donature $donature)
    {
        $this->validate($request, [
            'status' => 'required|integer|max:2',
        ]);

        $donature->status = $request->input('status');
        $donature->save();
        return ['message' => 'Data updated!'];
    }
}
