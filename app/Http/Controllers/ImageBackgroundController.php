<?php

namespace App\Http\Controllers;

use App\Models\ImageBackground;
use Illuminate\Http\Request;
use App\Transformers\Serializer\ArraySerializer;
use App\Transformers\ImageBackgroundTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Response;

class ImageBackgroundController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $takmir = Auth::user();
        $mosque_id = $takmir->mosque_id;

        if ($request->input('acp') == 'masjid') {
          $columns = ['', 'image', 'name', 'start_at', 'end_at', 'status'];

          $length = $request->input('length');
          $column = $request->input('column');
          $dir = $request->input('dir');
          $searchValue = $request->input('search');

          $query = ImageBackground::orderBy($columns[$column], $dir)
                                    ->where('mosque_id', $mosque_id);

          if ($request->input('layout')) {
              $query->where('layout', $request->input('layout'));
          }

          if ($searchValue) {
            $query->where(function($query) use ($searchValue) {
              $query->where('image', 'like', '%' . $searchValue . '%')
                    ->orWhere('name', 'like', '%' . $searchValue . '%')
                    ->orWhere('start_at', 'like', '%' . $searchValue . '%')
                    ->orWhere('end_at', 'like', '%' . $searchValue . '%')
                    ->orWhere('status', 'like', '%' . $searchValue . '%');
            });
          }

          $data = $query->paginate($length);
          return ['data' => $data, 'draw' => $request->input('draw')];
        } else {
          return [];
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $takmir = Auth::user();

        $this->validate($request, [
            'name' => 'required|string|max:255',
            'image' => 'required|image|max:5120',
            'information' => 'nullable|string',
            'start_at' => 'required|date|max:255',
            'end_at' => 'required|date|max:255',
            'layout' => 'required|int|max:2',
        ]);

        $alwaysShow = 0;
        if ($request['always_show']) {
          $alwaysShow = 1;
        }

        $path = $request->file('image')->storeAs('public/images/background', Str::random(10).time().'.'.$request->file('image')->getClientOriginalExtension());

        $view_layout = array();
        if ($request->post('view_layout') != 3) {
          array_push($view_layout, 1);
        }
        if ($request->post('view_layout') != 2) {
          array_push($view_layout, 2);
        }

        return ImageBackground::create([
            'mosque_id' => $takmir->mosque_id,
            'name' => $request['name'],
            'image' => env('APP_URL').'/storage/'.str_replace('public/', '', $path),
            'information' => $request['information'],
            'start_at' => $request['start_at'],
            'end_at' => $request['end_at'],
            'layout' => $request['layout'],
            'always_show' => $alwaysShow,
            'view_layout' => json_encode($view_layout),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ImageBackground  $imageBackground
     * @return \Illuminate\Http\Response
     */
    public function show(ImageBackground $imageBackground)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ImageBackground  $imageBackground
     * @return \Illuminate\Http\Response
     */
    public function edit(ImageBackground $imageBackground)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ImageBackground  $imageBackground
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = ImageBackground::findOrFail($id);
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'image' => 'nullable|image|max:5120',
            'information' => 'nullable|string',
            'start_at' => 'required|date|max:255',
            'end_at' => 'required|date|max:255',
            'layout' => 'required|int|max:2',
        ]);

        if ($request->hasFile('image')) {
            // $path = $request->file('image')->storeAs('public/images/background', Str::random(10).time().'.'.$request->file('image')->getClientOriginalExtension());
            $path = $request->file('image')->storeAs('public/images/background', Str::random(10).time().'.jpg');
            $data->image = env('APP_URL').'/storage/'.str_replace('public/', '', $path);
        }
        $data->name = $request->input('name');
        $data->information = $request->input('information');
        $data->start_at = $request->input('start_at');
        $data->end_at = $request->input('end_at');
        $data->layout = $request->input('layout');
        $data->always_show = 0;
        if ($request->input('always_show')) {
          $data->always_show = 1;
        }

        $view_layout = array();
        if ($request->input('view_layout') != 3) {
          array_push($view_layout, 1);
        }
        if ($request->post('view_layout') != 2) {
          array_push($view_layout, 2);
        }
        $data->view_layout = json_encode($view_layout);

        $data->save();

        return ['message' => 'Data updated!'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ImageBackground  $imageBackground
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = ImageBackground::findOrFail($id);
        $data->delete();
        return ['message' => 'Data Deleted!'];
    }

    public function status(Request $request, $id)
    {
        $data = ImageBackground::findOrFail($id);
        $this->validate($request, [
            'status' => 'required|integer|max:2',
        ]);

        $data->status = $request->input('status');
        $data->save();
        return ['message' => 'Data updated!'];
    }

    public function image(Request $request, $id)
    {
        $data = ImageBackground::findOrFail($id);
        $filename = str_replace(env('APP_URL').'/storage/', '', $data->image);
        $path = storage_path('app/public/' . $filename);

        if (!File::exists($path)) {
            abort(404);
        }

        $file = File::get($path);
        $type = File::mimeType($path);

        return response($file)
                ->header('Content-Type', $type);
    }
}
