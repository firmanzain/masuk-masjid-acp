<?php

namespace App\Http\Controllers;

use App\Models\Hijriah;
use Illuminate\Http\Request;

class HijriahCotroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Hijriah  $hijriah
     * @return \Illuminate\Http\Response
     */
    public function show(Hijriah $hijriah)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Hijriah  $hijriah
     * @return \Illuminate\Http\Response
     */
    public function edit(Hijriah $hijriah)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Hijriah  $hijriah
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Hijriah $hijriah)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Hijriah  $hijriah
     * @return \Illuminate\Http\Response
     */
    public function destroy(Hijriah $hijriah)
    {
        //
    }
}
