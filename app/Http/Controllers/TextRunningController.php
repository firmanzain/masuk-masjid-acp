<?php

namespace App\Http\Controllers;

use App\Models\TextRunning;
use Illuminate\Http\Request;
use App\Transformers\Serializer\ArraySerializer;
use App\Transformers\TextRunningTransformer;
use Illuminate\Support\Facades\Auth;

class TextRunningController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $takmir = Auth::user();
        $mosque_id = $takmir->mosque_id;

        if ($request->input('acp') == 'masjid') {
          $columns = ['', 'text', 'location', 'start_at', 'end_at', 'status'];

          $length = $request->input('length');
          $column = $request->input('column');
          $dir = $request->input('dir');
          $searchValue = $request->input('search');

          $query = TextRunning::orderBy($columns[$column], $dir)
                                ->where('mosque_id', $mosque_id);

          if ($request->input('layout')) {
              $query->where('location', $request->input('layout'));
          }

          if ($searchValue) {
            $query->where(function($query) use ($searchValue) {
              $query->where('text', 'like', '%' . $searchValue . '%')
                    ->orWhere('location', 'like', '%' . $searchValue . '%')
                    ->orWhere('start_at', 'like', '%' . $searchValue . '%')
                    ->orWhere('end_at', 'like', '%' . $searchValue . '%')
                    ->orWhere('status', 'like', '%' . $searchValue . '%');
            });
          }

          $data = $query->paginate($length);
          return ['data' => $data, 'draw' => $request->input('draw')];
        } else {
          return [];
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $takmir = Auth::user();

        $this->validate($request, [
            'text' => 'required|string',
            'location' => 'required|integer|max:2',
            'start_at' => 'required|date|max:255',
            'end_at' => 'required|date|max:255',
        ]);

        $alwaysShow = 0;
        if ($request['always_show']) {
          $alwaysShow = 1;
        }

        return TextRunning::create([
            'mosque_id' => $takmir->mosque_id,
            'text' => $request['text'],
            'location' => $request['location'],
            'start_at' => $request['start_at'],
            'end_at' => $request['end_at'],
            'always_show' => $alwaysShow,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TextRunning  $textRunning
     * @return \Illuminate\Http\Response
     */
    public function show(TextRunning $textRunning)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TextRunning  $textRunning
     * @return \Illuminate\Http\Response
     */
    public function edit(TextRunning $textRunning)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TextRunning  $textRunning
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = TextRunning::findOrFail($id);
        $this->validate($request, [
            'text' => 'required|string',
            'location' => 'required|integer|max:2',
            'start_at' => 'required|date|max:255',
            'end_at' => 'required|date|max:255',
        ]);

        $data->text = $request->input('text');
        $data->location = $request->input('location');
        $data->start_at = $request->input('start_at');
        $data->end_at = $request->input('end_at');
        $data->always_show = 0;
        if ($request->input('always_show')) {
          $data->always_show = 1;
        }
        $data->save();
        return ['message' => 'Data updated!'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TextRunning  $textRunning
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = TextRunning::findOrFail($id);
        $data->delete();
        return ['message' => 'Data Deleted!'];
    }

    public function status(Request $request, $id)
    {
        $data = TextRunning::findOrFail($id);
        $this->validate($request, [
            'status' => 'required|integer|max:2',
        ]);

        $data->status = $request->input('status');
        $data->save();
        return ['message' => 'Data updated!'];
    }
}
