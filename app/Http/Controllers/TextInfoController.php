<?php

namespace App\Http\Controllers;

use App\Models\TextInfo;
use Illuminate\Http\Request;
use App\Transformers\Serializer\ArraySerializer;
use App\Transformers\TextInfoTransformer;
use Illuminate\Support\Facades\Auth;

class TextInfoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $takmir = Auth::user();
        $mosque_id = $takmir->mosque_id;

        if ($request->input('acp') == 'masjid') {
          $columns = ['', 'text', 'start_at', 'end_at', 'status'];

          $length = $request->input('length');
          $column = $request->input('column');
          $dir = $request->input('dir');
          $searchValue = $request->input('search');

          $query = TextInfo::orderBy($columns[$column], $dir)
                             ->where('mosque_id', $mosque_id);

          if ($searchValue) {
            $query->where(function($query) use ($searchValue) {
              $query->where('text', 'like', '%' . $searchValue . '%')
                    ->orWhere('start_at', 'like', '%' . $searchValue . '%')
                    ->orWhere('end_at', 'like', '%' . $searchValue . '%')
                    ->orWhere('status', 'like', '%' . $searchValue . '%');
            });
          }

          $data = $query->paginate($length);
          return ['data' => $data, 'draw' => $request->input('draw')];
        } else {
          return [];
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $takmir = Auth::user();

        $this->validate($request, [
            'text' => 'required|string',
            'start_at' => 'required|date|max:255',
            'end_at' => 'required|date|max:255',
        ]);

        return TextInfo::create([
            'mosque_id' => $takmir->mosque_id,
            'text' => $request['text'],
            'start_at' => $request['start_at'],
            'end_at' => $request['end_at'],
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TextInfo  $textInfo
     * @return \Illuminate\Http\Response
     */
    public function show(TextInfo $textInfo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TextInfo  $textInfo
     * @return \Illuminate\Http\Response
     */
    public function edit(TextInfo $textInfo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TextInfo  $textInfo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = TextInfo::findOrFail($id);
        $this->validate($request, [
            'text' => 'required|string',
            'start_at' => 'required|date|max:255',
            'end_at' => 'required|date|max:255',
        ]);

        $data->update($request->all());
        return ['message' => 'Data updated!'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TextInfo  $textInfo
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = TextInfo::findOrFail($id);
        $data->delete();
        return ['message' => 'Data Deleted!'];
    }

    public function status(Request $request, $id)
    {
        $data = TextInfo::findOrFail($id);
        $this->validate($request, [
            'status' => 'required|integer|max:2',
        ]);

        $data->status = $request->input('status');
        $data->save();
        return ['message' => 'Data updated!'];
    }
}
