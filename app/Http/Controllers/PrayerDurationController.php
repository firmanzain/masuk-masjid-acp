<?php

namespace App\Http\Controllers;

use App\Models\PrayerDuration;
use Illuminate\Http\Request;
use App\Transformers\Serializer\ArraySerializer;
use App\Transformers\PrayerDurationTransformer;
use Illuminate\Support\Facades\Auth;

class PrayerDurationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $takmir = Auth::user();
        $mosque_id = $takmir->mosque_id;

        if ($request->input('acp') == 'masjid') {

          $data = PrayerDuration::where('mosque_id', $mosque_id)->get();

          return fractal()
                ->collection($data, new PrayerDurationTransformer())
                ->serializeWith(new ArraySerializer())
                ->toArray();
        } else {
          return [];
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PrayerDuration  $prayerDuration
     * @return \Illuminate\Http\Response
     */
    public function show(PrayerDuration $prayerDuration)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PrayerDuration  $prayerDuration
     * @return \Illuminate\Http\Response
     */
    public function edit(PrayerDuration $prayerDuration)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PrayerDuration  $prayerDuration
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = PrayerDuration::findOrFail($id);
        $this->validate($request, [
            'start_at' => 'required|string|max:255',
            'azan_duration' => 'required|string|max:255',
            'iqamat_duration' => 'required|string|max:255',
            'solat_duration' => 'required|string|max:255',
        ]);

        $data->update($request->all());
        return ['message' => 'Data updated!'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PrayerDuration  $prayerDuration
     * @return \Illuminate\Http\Response
     */
    public function destroy(PrayerDuration $prayerDuration)
    {
        //
    }
}
