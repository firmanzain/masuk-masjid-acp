<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\ImageBackground;

class ImageBackgroundTransformer extends TransformerAbstract
{
    protected $device;

    public function __construct($device = null) {
        $this->device = $device;
    }

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(ImageBackground $data)
    {

        // $view_layout = json_decode($data->view_layout);
        // $show_slide = 0;
        // $show_full = 0;
        // for ($i = 0; $i < sizeof($view_layout); $i++) {
        //     if ($view_layout[$i] == "slide") {
        //         $show_slide = 1;
        //     }
        //     if ($view_layout[$i] == "full") {
        //         $show_full = 1;
        //     }
        // }

        $response = [
            'id' => $data->id,
            'image' => $data->image,
            'video' => $data->video,
            'view_layout' => json_decode($data->view_layout),
            'start_at' => $data->start_at,
            'end_at' => ($data->always_show == 0) ? $data->end_at : date('Y-m-d', strtotime(date('Y-m-d') . ' +5 year')),
            // 'show_slide' => ($show_slide == 1) ? true : false,
            // 'show_full' => ($show_full == 1) ? true : false,
        ];

        if (!$this->device) {
            $response['name'] = $data->name;
            $response['information'] = $data->information;
            $response['status'] = $data->status;
        }

        return $response;
    }
}
