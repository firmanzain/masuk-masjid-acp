<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\ImagePhoto;

class ImagePhotoTransformer extends TransformerAbstract
{
    protected $device;

    public function __construct($device = null) {
        $this->device = $device;
    }

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(ImagePhoto $data)
    {
        $response = [
            'id' => $data->id,
            'image' => $data->image,
            'start_at' => $data->start_at,
            'end_at' => $data->end_at,
        ];

        if (!$this->device) {
            $response['name'] = $data->name;
            $response['information'] = $data->information;
            $response['status'] = $data->status;
        }

        return $response;
    }
}
