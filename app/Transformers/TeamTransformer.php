<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Team, App\Models\BankAccount, App\Models\TeamBalance, App\Models\TeamWithdraw;
use Illuminate\Support\Facades\DB;

class TeamTransformer extends TransformerAbstract
{
    protected $token;

    public function __construct($token = null) {
        $this->token = $token;
    }

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'bankaccount'
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Team $data)
    {
        $balance = TeamBalance::where('team_id', $data->id)->sum('nominal');
        $withdraw = TeamWithdraw::select(DB::raw('SUM(nominal + admin) as nominal'))
                                  ->where('team_id', $data->id)->where('status', '!=', 0)
                                  ->first();

        $nominal = $balance - $withdraw->nominal;

        if (!$this->token) {
            return [
                'id' => $data->id,
                'name' => $data->name,
                'card_id' => $data->card_id,
                'born_place' => $data->born_place,
                'born_date' => $data->born_date,
                'email' => $data->email,
                'occupation' => $data->occupation,
                'image' => $data->image,
                'address' => $data->address,
                'balance' => $nominal,
            ];
        } else {
            return [
                'id' => $data->id,
                'name' => $data->name,
                'card_id' => $data->card_id,
                'born_place' => $data->born_place,
                'born_date' => $data->born_date,
                'email' => $data->email,
                'occupation' => $data->occupation,
                'image' => $data->image,
                'address' => $data->address,
                'balance' => $nominal,
                'token' => (isset($data->token)) ? $data->token : NULL,
            ];
        }
    }

    public function includeBankaccount(Team $data) {
        $data = BankAccount::where('id', $data->id)->first();
        if (!$data) {
            return NULL;
        }

        return $this->item($data, new BankAccountTransformer());
    }
}
