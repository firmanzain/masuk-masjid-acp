<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;

class AllInfoTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform($data)
    {
        $response = [
            'id' => $data['id'],
            'title' => $data['title'],
            'text' => $data['text'],
            'details' => $data['details'],
            'start_at' => $data['start_at'],
            'end_at' => $data['end_at'],
            'date' => $data['date'],
        ];

        return $response;
    }
}
