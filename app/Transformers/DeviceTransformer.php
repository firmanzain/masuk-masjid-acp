<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Device, App\Models\Mosque;
use Carbon\Carbon;

class DeviceTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        // 'mosque',
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Device $data)
    {
        return [
            'id' => $data->id,
            'number' => $data->number,
            'name' => $data->name,
            'type' => $data->type,
            'last_sync' => Carbon::parse($data->last_sync),
            'application_version' => $data->application_version,
            'token' => ($data->token) ? $data->token : null,
            'status' => $data->status,
        ];
    }

    public function includeMosque(Device $data) {
        $data = Mosque::where('id', $data->mosque_id)->first();
        if (!$data) {
            return NULL;
        }

        return $this->item($data, new MosqueTransformer());
    }
}
