<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\TextInfo;
use Carbon\Carbon;

class TextInfoTransformer extends TransformerAbstract
{
    protected $device;

    public function __construct($device = null) {
        $this->device = $device;
    }

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(TextInfo $data)
    {
        $response = [
            'id' => $data->id,
            'text' => $data->text,
            'start_at' => $data->start_at,
            'end_at' => $data->end_at,
        ];

        if (!$this->device) {
            $response['status'] = $data->status;
        }

        return $response;
    }
}
