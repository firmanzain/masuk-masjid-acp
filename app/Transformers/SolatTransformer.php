<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Solat;
use Carbon\Carbon;

class SolatTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Solat $data)
    {
        return [
            'id' => strtolower($data->name)."-".strtotime($data->dateTime),
            'name' => $data->name,
            'dateTime' => Carbon::parse($data->dateTime),
            'isAdhan' => ($data->isAdhan == 1) ? true : false,
        ];
    }
}
