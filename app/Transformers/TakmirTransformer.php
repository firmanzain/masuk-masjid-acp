<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Takmir;
use Carbon\Carbon;
use App\Models\Mosque;

class TakmirTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'mosque',
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Takmir $data)
    {
        return [
            'id' => $data->id,
            'name' => $data->name,
            'username' => $data->username,
            'token' => $data->token,
        ];
    }

    public function includeMosque(Takmir $data) {
        $data = Mosque::where('id', $data->mosque_id)->first();
        if (!$data) {
            return NULL;
        }

        return $this->item($data, new MosqueTransformer());
    }
}
