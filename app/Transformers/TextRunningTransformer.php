<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\TextRunning;
use Carbon\Carbon;

class TextRunningTransformer extends TransformerAbstract
{
    protected $device;

    public function __construct($device = null) {
        $this->device = $device;
    }

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(TextRunning $data)
    {
        $response = [
            'id' => $data->id,
            'text' => $data->text,
            'location' => $data->location,
            'start_at' => $data->start_at,
            'end_at' => ($data->always_show == 0) ? $data->end_at : date('Y-m-d', strtotime(date('Y-m-d') . ' +5 year')),
        ];

        if (!$this->device) {
            $response['status'] = $data->status;
        }

        return $response;
    }
}
