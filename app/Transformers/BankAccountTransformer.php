<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\BankAccount, App\Models\Bank;

class BankAccountTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'bank',
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(BankAccount $data)
    {
        return [
            'id' => $data->id,
            'name' => $data->name,
            'number' => $data->number,
        ];
    }

    public function includeBank(BankAccount $data) {
        $data = Bank::where('id', $data->bank_id)->first();
        if (!$data) {
            return NULL;
        }

        return $this->item($data, new BankTransformer());
    }
}
