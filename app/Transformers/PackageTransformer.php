<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Package;

class PackageTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Package $data)
    {
        return [
            'id' => $data->id,
            'name' => $data->name,
            'count' => ($data->count != 0) ? $data->count : 'Tidak Terbatas',
            'count_number' => $data->count,
            'price' => $data->price,
            'month' => $data->month,
        ];
    }
}
