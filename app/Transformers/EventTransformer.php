<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Event, App\Models\EventItem;

class EventTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'item'
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Event $data)
    {
        return [
            'id' => $data->id,
            'date' => $data->date,
            'title' => $data->title,
            'status' => $data->status,
        ];
    }

    public function includeItem(Event $data) {
        $data = EventItem::where('event_id', $data->id)->get();
        if (!$data) {
            return NULL;
        }

        return $this->item($data, new EventItemTransformer());
    }
}
