<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Donature;

class DonatureTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Donature $data)
    {
        return [
            'id' => $data->id,
            'date' => $data->date,
            'name' => $data->name,
            'address' => $data->address,
            'amount' => $data->amount,
            'information' => $data->information,
            'status' => $data->status,
        ];
    }
}
