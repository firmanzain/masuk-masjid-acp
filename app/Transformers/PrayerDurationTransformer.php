<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\PrayerDuration;

class PrayerDurationTransformer extends TransformerAbstract
{
    protected $device;

    public function __construct($device = null) {
        $this->device = $device;
    }

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(PrayerDuration $data)
    {
        if (!$this->device) {
            return [
                'id' => $data->id,
                'name' => $data->name,
                'start_at' => $data->start_at,
                'azan_duration' => $data->azan_duration,
                'iqamat_duration' => $data->iqamat_duration,
                'solat_duration' => $data->solat_duration,
            ];
        } else {

            $azan_duration = null;
            if ($data->azan_duration != null) {
              $h = intval(date('H', strtotime($data->azan_duration))) * 60 * 60 * 1000;
              $m = intval(date('i', strtotime($data->azan_duration))) * 60 * 1000;
              $s = intval(date('s', strtotime($data->azan_duration))) * 1000;
              $azan_duration = $h + $m + $s;
            }

            $iqamat_duration = null;
            if ($data->iqamat_duration != null) {
              $h = intval(date('H', strtotime($data->iqamat_duration))) * 60 * 60 * 1000;
              $m = intval(date('i', strtotime($data->iqamat_duration))) * 60 * 1000;
              $s = intval(date('s', strtotime($data->iqamat_duration))) * 1000;
              $iqamat_duration = $h + $m + $s;
            }

            $solat_duration = null;
            if ($data->solat_duration != null) {
              $h = intval(date('H', strtotime($data->solat_duration))) * 60 * 60 * 1000;
              $m = intval(date('i', strtotime($data->solat_duration))) * 60 * 1000;
              $s = intval(date('s', strtotime($data->solat_duration))) * 1000;
              $solat_duration = $h + $m + $s;
            }

            return [
              'id' => $data->id,
              'name' => $data->name,
              'start_at' => ($data->start_at != '00:00:00') ? $data->start_at : null,
              'azan_duration' => $azan_duration,
              'iqamat_duration' => $iqamat_duration,
              'solat_duration' => $solat_duration,
            ];
        }
    }
}
