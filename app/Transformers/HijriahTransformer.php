<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Hijriah;

class HijriahTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Hijriah $data)
    {
        return [
            'id' => $data->date . '-' . str_pad($data->month, 2, '0', STR_PAD_LEFT) . '-' . $data->year,
            'adDate' => $data->adDate,
            'date' => $data->date,
            'month' => $data->month_name,
            'year' => $data->year,
        ];
    }
}
