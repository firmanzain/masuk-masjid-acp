<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Province;

class ProvinceTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Province $data)
    {
        return [
            'id' => $data->id,
            'name' => $data->name,
            'code' => $data->code,
        ];
    }
}
