<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Preacher;

class PreacherTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Preacher $data)
    {
        return [
            'id' => $data->id,
            'date' => $data->date,
            'khotib' => $data->khotib,
            'bilal' => $data->bilal,
            'status' => $data->status,
        ];
    }
}
