<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Mosque;
use Carbon\Carbon;
use App\Models\Province, App\Models\Preacher, App\Models\Donature,
App\Models\TextRunning, App\Models\TextInfo, App\Models\TextArabic,
App\Models\ImageBackground, App\Models\PrayerDuration, App\Models\Package,
App\Models\Event, App\Models\EventItem, App\Models\Configuration,
App\Models\Solat, App\Models\Team;

class MosqueTransformer extends TransformerAbstract
{

    protected $defaultIncludes = [
        'province', 'package', 'team',
    ];

    protected $availableIncludes = [
        'khotib', 'donature', 'runningtext', 'info', 'source', 'slideshow', 'solat'
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Mosque $data)
    {
        $version = 0;
        $confVersion = Configuration::where('key', 'version')->first();
        if ($confVersion) {
            $version = intval($confVersion->value);
        }

        $show_information = json_decode($data->show_information);
        $show_name = 0;
        $show_logo = 0;
        $show_address = 0;
        for ($i = 0; $i < sizeof($show_information); $i++) {
            if ($show_information[$i] == "name") {
                $show_name = 1;
            }
            if ($show_information[$i] == "logo") {
                $show_logo = 1;
            }
            if ($show_information[$i] == "address") {
                $show_address = 1;
            }
        }

        return [
            'id' => $data->id,
            'number' => $data->number,
            'logo' => ($data->logo) ? $data->logo : env('APP_URL').'/storage/images/logo/default.png',
            'name' => $data->name,
            'address' => $data->address,
            'latitude' => floatval($data->latitude),
            'longitude' => floatval($data->longitude),
            'last_subscribe' => Carbon::parse($data->last_subscribe),
            'show_ads' => ($data->show_ads == 1) ? true : false,
            'version' => $version,
            'status' => $data->status,
            'show_name' => ($show_name == 1) ? true : false,
            'show_logo' => ($show_logo == 1) ? true : false,
            'show_address' => ($show_address == 1) ? true : false,
        ];
    }

    public function includeTeam(Mosque $data) {
        $data = Team::where('id', $data->team_id)->first();
        if (!$data) {
            return NULL;
        }

        return $this->item($data, new TeamTransformer());
    }

    public function includeProvince(Mosque $data) {
        $data = Province::where('id', $data->province_id)->first();
        if (!$data) {
            return NULL;
        }

        return $this->item($data, new ProvinceTransformer());
    }

    public function includePackage(Mosque $data) {
        $data = Package::where('id', $data->package_id)->first();
        if (!$data) {
            return NULL;
        }

        return $this->item($data, new PackageTransformer());
    }

    public function includeRunningtext(Mosque $data) {
        $data = TextRunning::where('mosque_id', $data->id)
                             ->whereDate('end_at', '>=', date('Y-m-d'))
                             ->where('location', $data->layout)
                             ->where('status', 1)->get();
        if (!$data) {
            return NULL;
        }

        return $this->collection($data, new TextRunningTransformer(['device']));
    }

    public function includeInfo(Mosque $data) {
        $info = array();

        if ($data->layout == 1) {
            return $this->collection($info, new AllInfoTransformer());
        }


        $preacherQuery = Preacher::where('mosque_id', $data->id)
                          ->whereDate('date', '>=', date('Y-m-d'))
                          ->where('status', 1)->get();
        if ($preacherQuery) {
            foreach ($preacherQuery as $value) {
                $text = array(
                    'id' => 'k'.$value->id,
                    'title' => "Khotib & Bilal",
                    'text' => "Khotib & Bilal\nKhotib: ".$value->khotib."\nBilal: ".$value->bilal,
                    'details' => array(
                        'khotib' => $value->khotib,
                        'bilal' => $value->bilal,
                    ),
                    'start_at' => date('Y-m-d', strtotime($value->date . ' -1 day')),
                    'end_at' => ($value->always_show == 0) ? $value->date : date('Y-m-d', strtotime(date('Y-m-d') . ' +5 year')),
                    'date' => $value->date,
                );
                array_push($info, $text);
            }
        }

        $donatureQuery = Donature::where('mosque_id', $data->id)
                          ->whereMonth('date', '>=', date('m'))
                          ->where('status', 1)->get();
        if ($donatureQuery) {
            foreach ($donatureQuery as $value) {

                $detail = "Donasi\nNama: ".$value->name."\nNominal: Rp".number_format($value->amount,2,",",".");

                $text = array(
                    'id' => 'd'.$value->id,
                    'title' => "Donasi",
                    'text' => $detail,
                    'details' => array(
                        'nama' => $value->name,
                        'nominal' => "Rp".number_format($value->amount,2,",","."),
                    ),
                    'start_at' => date('Y-m-d', strtotime($value->date . ' -1 day')),
                    'end_at' => ($value->always_show == 0) ? date('Y-m-t', strtotime($value->date)) : date('Y-m-d', strtotime(date('Y-m-d') . ' +5 year')),
                    'date' => $value->date,
                );
                array_push($info, $text);
            }
        }

        $eventQuery = Event::where('mosque_id', $data->id)
                          ->whereDate('date', '>=', date('Y-m-d'))
                          ->where('status', 1)->get();
        if ($eventQuery) {
            foreach ($eventQuery as $value) {

                $item = "";
                $details = array();
                $eventDetailQuery = EventItem::where('event_id', $value->id)->get();
                $eventDetailQueryCount = EventItem::where('event_id', $value->id)->count();
                $count = 0;
                if ($eventDetailQuery) {
                  foreach ($eventDetailQuery as $valueItem) {
                    $details[strtolower($valueItem->key)] = $valueItem->value;
                    $item .= $valueItem->key.": ".$valueItem->value;
                    $count++;
                    if ($eventDetailQueryCount == $count) {
                    } else {
                      $item .= "\n";
                    }
                  }
                }

                $event = array(
                    'id' => 'e'.$value->id,
                    'title' => $value->title,
                    'text' => $value->title."\n".$item,
                    'details' => $details,
                    'start_at' => date('Y-m-d', strtotime($value->show_at . ' -1 day')),
                    'end_at' => ($value->always_show == 0) ? date('Y-m-d',strtotime($value->date . "+1 days")) : date('Y-m-d', strtotime(date('Y-m-d') . ' +5 year')),
                    'date' => $value->date,
                );
                array_push($info, $event);
            }
        }

        usort($info, function($a, $b) {
            return $a['date'] <=> $b['date'];
        });

        return $this->collection($info, new AllInfoTransformer());
    }

    public function includeSource(Mosque $data) {
        $data = TextArabic::where('mosque_id', $data->id)
                            ->whereDate('end_at', '>=', date('Y-m-d'))
                            ->where('status', 1)->get();
        if (!$data) {
            return NULL;
        }

        return $this->collection($data, new TextArabicTransformer(['device']));
    }

    public function includeSlideshow(Mosque $data) {
        $data = ImageBackground::where('mosque_id', $data->id)
                                 ->whereDate('end_at', '>=', date('Y-m-d'))
                                 ->where('layout', $data->layout)
                                 ->where('status', 1)->get();
        if (!$data) {
            return NULL;
        }

        return $this->collection($data, new ImageBackgroundTransformer(['device']));
    }

    public function includeSolat(Mosque $data) {
        $data = PrayerDuration::where('mosque_id', $data->id)->get();
        if (!$data) {
            return NULL;
        }

        return $this->collection($data, new PrayerDurationTransformer(['device']));
    }
}
