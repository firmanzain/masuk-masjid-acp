<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use GuzzleHttp\Client, GuzzleHttp\Exception\ClientException;
use App\Models\Mosque, App\Models\Hijriah, App\Models\Solat,
App\Models\Team, App\Models\TeamBalance, App\Models\Package,
App\Models\Device;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

        $schedule->call(function () {

            $client = new Client();
            $mosques = Mosque::get();
            $date = date('Y-m-01');

            if ($mosques) {
                foreach ($mosques as $mosque) {
                    for ($i = 1; $i < 4; $i++) {

                      $date = date('Y-m-01', strtotime($date . ' +1 month'));

                      $check = Solat::where('mosque_id', $mosque->id)
                                      ->whereMonth('dateTime', date('m', strtotime($date)))
                                      ->whereYear('dateTime', date('Y', strtotime($date)))
                                      ->first();
                      if ($check) {
                          continue;
                      }

                      // DELETE OLD RECORDS
                      $deleteHijriah = Hijriah::where('adDate', '<', $date)->delete();
                      $deleteSolat = Solat::whereDate('dateTime', '<', $date)->delete();

                      try {
                          $response = $client->request('GET', env('API_ADHAN'), [
                              'query' => [
                                'latitude' => $mosque->latitude,
                                'longitude' => $mosque->longitude,
                                'month' => date('m', strtotime($date)),
                                'year' => date('Y', strtotime($date)),
                                'method' => "99",
                                'methodSettings' => "19.5,1.5,18.5",
                                'tune' => "0,0,-2,3,2,0,0,0,39",
                              ]
                          ]);

                          $data = json_decode($response->getBody());

                          foreach ($data->data as $result) {

                              $date = date('Y-m-d', strtotime($result->date->gregorian->year.'-'.$result->date->gregorian->month->number.'-'.$result->date->gregorian->day));

                              // CHECK HIJRIAH CALENDER
                              $checkHijriah = Hijriah::where('adDate', $date)->first();
                              if (!$checkHijriah) {
                                  $hijriah = new Hijriah();
                                  $hijriah->id = $result->date->hijri->date;
                                  $hijriah->adDate = $date;
                                  $hijriah->date = $result->date->hijri->day;
                                  $hijriah->month = $result->date->hijri->month->number;
                                  $hijriah->month_name = $result->date->hijri->month->en;
                                  $hijriah->year = $result->date->hijri->year;
                                  $hijriah->save();
                              }

                              foreach ($result->timings as $key => $value) {
                                  $id = "";
                                  $name = "";
                                  $isAdhan = true;

                                  if ($key == "Fajr") {
                                      $id = "shubuh-";
                                      $name = "Shubuh";
                                  } else if ($key == "Dhuhr") {
                                      $id = "dzuhur-";
                                      $name = "Dzuhur";
                                  } else if ($key == "Asr") {
                                      $id = "ashar-";
                                      $name = "Ashar";
                                  } else if ($key == "Maghrib") {
                                      $id = "magrib-";
                                      $name = "Maghrib";
                                  } else if ($key == "Isha") {
                                      $id = "isya-";
                                      $name = "Isha";
                                  } else if ($key == "Imsak") {
                                      $id = "imsak-";
                                      $name = "Imsak";
                                      $isAdhan = false;
                                  } else if ($key == "Sunrise") {
                                      $id = "terbit-";
                                      $name = "Terbit";
                                      $isAdhan = false;
                                  } else {
                                      continue;
                                  }

                                  $solat = new Solat();
                                  $solat->id = $id . strtotime($date);
                                  $solat->mosque_id = $mosque->id;
                                  $solat->name = $name;
                                  $solat->dateTime = $date . " " . substr($value, 0, 5);
                                  $solat->isAdhan = $isAdhan;
                                  $solat->save();
                              }
                          }
                      } catch (ClientException $e) {
                          continue;
                      }
                    }
                }
            }

        })->monthly(date('t'), '23:00');

        $schedule->call(function () {

            $mosques = Mosque::where('team_id', '!=', 0)->get();

            if ($mosques) {
                foreach ($mosques as $mosque) {

                    $device = Device::where('mosque_id', $mosque->id)
                                      ->where('status_confirmation', 2)
                                      ->count();

                    if ($device != 0) {
                      $checkBalance = TeamBalance::where('team_id', $mosque->team_id)
                                                   ->where('information', 'like', '%Device%')
                                                   ->where('information', 'like', $mosque->name)
                                                   ->whereMonth('date', date('m'))
                                                   ->whereYear('date', date('Y'))
                                                   ->first();

                      if (!$checkBalance) {
                          $balance = new TeamBalance();
                          $balance->team_id = $mosque->team_id;
                          $balance->date = $date('Y-m-d');
                          $balance->nominal = $device * 5000;
                          $balance->information = 'Fee Bulanan '.$mosque->name.' '.$device.' Device';
                          $balance->save();
                      }
                    }
                }
            }

        })->monthlyOn(2, '23:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
