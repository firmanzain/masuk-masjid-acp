<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TakmirToken extends Model
{

    protected $fillable = [
       'takmir_id', 'token',
    ];
}
