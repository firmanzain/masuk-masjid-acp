<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ImageBackground extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $fillable = [
      'mosque_id', 'name', 'image', 'video', 'information', 'start_at', 'end_at', 'status', 'layout', 'always_show', 'view_layout',
    ];
}
