<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Mosque extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $fillable = [
      'number', 'name', 'province_id', 'address', 'latitude', 'longitude', 'last_subscribe', 'package_id', 'show_ads', 'status', 'show_information', 'team_id'
    ];

    public function province()
    {
        return $this->hasOne('App\Models\Province', 'id', 'province_id');
    }

    public function takmir()
    {
        return $this->hasMany('App\Models\Takmir');
    }
}
