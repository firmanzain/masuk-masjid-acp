<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TeamWithdraw extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $fillable = [
      'team_id', 'bank_account_id', 'nominal', 'information', 'status', 'admin',
    ];

    public function team()
    {
        return $this->hasOne('App\Models\Team', 'id', 'team_id');
    }
}
