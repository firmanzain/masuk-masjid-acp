<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Hijriah extends Model
{
    protected $fillable = [
      'id', 'adDate', 'date', 'month', 'month_name', 'year',
    ];
}
