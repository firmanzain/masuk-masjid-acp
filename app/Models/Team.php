<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\SoftDeletes;

class Team extends Authenticatable
{
    use HasApiTokens;
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $fillable = [
       'name', 'card_id', 'born_place', 'born_date', 'email', 'password', 'occupation', 'image', 'address'
    ];

    public function balance()
    {
        return $this->hasMany('App\Models\TeamBalance');
    }

    public function withdraw()
    {
        return $this->hasMany('App\Models\TeamWithdraw')->where('status', '!=', 0);
    }
}
