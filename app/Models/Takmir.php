<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\SoftDeletes;

class Takmir extends Authenticatable
{
    use HasApiTokens;
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $fillable = [
       'mosque_id', 'name', 'username', 'password',
    ];
}
