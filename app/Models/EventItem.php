<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EventItem extends Model
{
    protected $fillable = [
      'event_id', 'key', 'value',
    ];
}
