<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PrayerDuration extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $fillable = [
      'mosque_id', 'name', 'start_at', 'azan_duration', 'iqamat_duration', 'solat_duration',
    ];
}
