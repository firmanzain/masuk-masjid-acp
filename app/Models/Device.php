<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\SoftDeletes;

class Device extends Authenticatable
{
    use HasApiTokens;
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $fillable = [
       'mosque_id', 'number', 'name', 'type', 'status_confirmation', 'otp', 'otp_valid', 'last_sync', 'status', 'application_version',
    ];
}
