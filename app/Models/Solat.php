<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Solat extends Model
{
    protected $fillable = [
      'id', 'mosque_id', 'name', 'dateTime', 'isAdhan',
    ];
}
