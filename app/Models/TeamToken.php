<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TeamToken extends Model
{

    protected $fillable = [
       'team_id', 'token',
    ];
}
