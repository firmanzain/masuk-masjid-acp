<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $fillable = [
      'mosque_id', 'date', 'title', 'status', 'show_at', 'always_show',
    ];

    public function items()
    {
        return $this->hasMany('App\Models\EventItem');
    }
}
