@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="card card-primary card-outline">
          <div class="card-header">
          </div><!-- /.card-header -->
          <div class="card-body">
              @if (isset($package))
                  <form method="POST" action="{{ route('package.update', $package->id) }}">
                    @csrf
                    @method('PUT')
                    <div class="form-group row">
                      <label for="name" class="col-sm-2 col-form-label">Nama Paket</label>
                      <div class="col-sm-4">
                        <input type="text" class="form-control" name="name" id="name" placeholder="Nama Paket" value="{{ $package->name }}" required>
                      </div>
                      <label for="count" class="col-sm-2 col-form-label">Jumlah Device</label>
                      <div class="col-sm-4">
                        <input type="text" class="form-control" name="count" id="count" placeholder="Jumlah Device" value="{{ $package->count }}" onkeyup="checkPrice(event)" required>
                      </div>
                    </div>
                    <div class="form-group row">
                        <label for="month" class="col-sm-2 col-form-label">Lama Bulan Langganan</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" name="month" id="month" placeholder="Lama Bulan Langganan" value="{{ number_format($package->month, 0, '.', ',') }}" onkeyup="checkPrice(event)" required>
                        </div>
                        <label for="price" class="col-sm-2 col-form-label">Harga</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" name="price" id="price" placeholder="Harga" value="{{ number_format($package->price, 0, '.', ',') }}" onkeyup="checkPrice(event)" required>
                        </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-sm-12 text-right">
                          <a href="{{ route('package.index') }}" class="btn btn-secondary">
                              Batal
                          </a>
                          <button type="submit" class="btn btn-primary">Simpan</button>
                      </div>
                    </div>
                  </form>
              @else
                  <form method="POST" action="{{ route('package.store') }}">
                    @csrf
                    @method('POST')
                    <div class="form-group row">
                      <label for="name" class="col-sm-2 col-form-label">Nama Paket</label>
                      <div class="col-sm-4">
                        <input type="text" class="form-control" name="name" id="name" placeholder="Nama Paket" required>
                      </div>
                      <label for="count" class="col-sm-2 col-form-label">Jumlah Device</label>
                      <div class="col-sm-4">
                        <input type="text" class="form-control" name="count" id="count" placeholder="Jumlah Device" onkeyup="checkPrice(event)" required>
                      </div>
                    </div>
                    <div class="form-group row">
                        <label for="month" class="col-sm-2 col-form-label">Lama Bulan Langganan</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" name="month" id="month" placeholder="Lama Bulan Langganan" onkeyup="checkPrice(event)" required>
                        </div>
                        <label for="price" class="col-sm-2 col-form-label">Harga</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" name="price" id="price" placeholder="Harga" onkeyup="checkPrice(event)" required>
                        </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-sm-12 text-right">
                          <a href="{{ route('package.index') }}" class="btn btn-secondary">
                              Batal
                          </a>
                          <button type="submit" class="btn btn-primary">Simpan</button>
                      </div>
                    </div>
                  </form>
              @endif
          </div><!-- /.card-body -->
        </div>
        <!-- /.nav-tabs-custom -->
      </div>
      <!-- /.col -->
    </div>
</div>

<script type="text/javascript">

    function checkPrice(event) {
        let formatted = numeral(event.target.value).format('0,0')
        document.getElementById(event.target.id).value = formatted
    }
</script>
@endsection
