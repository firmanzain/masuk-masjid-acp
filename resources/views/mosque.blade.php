@extends('layouts.app')

@section('content')
<div class="container">

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
                Masjid
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item">
                  <a href="{{ url('/') }}">
                      Home
                  </a>
              </li>
              <li class="breadcrumb-item active">
                  Masjid
              </li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <div class="row">
      <div class="col-md-12">
        <div class="card card-primary card-outline">
            <div class="card-header">
                <div class="card-tools">
                    <a href="{{ url('/mosque/create') }}" class="btn btn-primary">
                        <i class="fas fa-plus-circle"></i>&nbsp;&nbsp; Tambah Data
                    </a>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
            @if (session('alert'))
                <div class="alert {!! session('alert') !!} alert-dismissible fade show" role="alert">
                    {!! session('message') !!}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No.</th>
                  <th>Nomor</th>
                  <th>Nama Masjid</th>
                  <th>Alamat</th>
                  <th>Terakhir Langganan</th>
                  <th style="width: 35%;"></th>
                </tr>
                </thead>
                <tbody>
                    @for ($i = 0; $i < sizeof($mosque); $i++)
                        <tr>
                          <td>
                              {{ ($i+1) }}
                          </td>
                          <td>
                              {{ $mosque[$i]['number'] }}
                          </td>
                          <td>
                              {{ $mosque[$i]['name'] }}
                          </td>
                          <td>
                              {{ $mosque[$i]['address'] }}
                          </td>
                          <td>
                              {{ date('d/m/Y', strtotime($mosque[$i]['last_subscribe'])) }}
                          </td>
                          <td class="text-center">
                              <a href="{{ url('/mosque/'.$mosque[$i]['id'].'/edit') }}" class="btn btn-sm btn-primary">
                                  <i class="fas fa-edit"></i>&nbsp;&nbsp; Ubah
                              </a>
                             <button type="button" class="btn btn-sm btn-primary" onclick="renewSubscribe({{ $mosque[$i]['id'] }})">
                                 <i class="fas fa-rss-square"></i> Perbarui Langganan
                             </button>
                             <button type="button" class="btn btn-sm btn-danger" onclick="deleteData({{ $mosque[$i]['id'] }})">
                                 <i class="fas fa-trash-alt"></i> Hapus
                             </button>
                          </td>
                        </tr>
                    @endfor
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
</div>

<script type="text/javascript">

    $('table').DataTable();

    function deleteData(id) {
        Swal.fire({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
          if (result.value) {
              axios.delete('/mosque/'+id, {})
              .then(function (response) {
                 location.reload();
              })
              .catch(function (error) {
                 Swal.fire(
                     'Alert!',
                     'Something went wrong.',
                     'warning'
                 )
              });
          }
        })
    }

    function renewSubscribe(id) {
        Swal.fire({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, renew subscribe!'
        }).then((result) => {
          if (result.value) {
              axios.post('/mosque/renew-subscribe/'+id, {})
              .then(function (response) {
                 location.reload();
              })
              .catch(function (error) {
                 Swal.fire(
                     'Alert!',
                     'Something went wrong.',
                     'warning'
                 )
              });
          }
        })
    }
</script>
@endsection
