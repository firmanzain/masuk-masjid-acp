@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="card card-primary card-outline">
          <div class="card-header">
          </div><!-- /.card-header -->
          <div class="card-body">
              @if (isset($bank))
                  <form method="POST" action="{{ route('bank.update', $bank->id) }}">
                    @csrf
                    @method('PUT')
                    <div class="form-group row">
                      <label for="name" class="col-sm-4 col-form-label">Nama Bank</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="name" id="name" placeholder="Nama Bank" value="{{ $bank->name }}" required>
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-sm-12 text-right">
                          <a href="{{ route('bank.index') }}" class="btn btn-secondary">
                              Batal
                          </a>
                          <button type="submit" class="btn btn-primary">Simpan</button>
                      </div>
                    </div>
                  </form>
              @else
                  <form method="POST" action="{{ route('bank.store') }}">
                    @csrf
                    @method('POST')
                    <div class="form-group row">
                      <label for="name" class="col-sm-4 col-form-label">Nama Bank</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="name" id="name" placeholder="Nama Bank" required>
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-sm-12 text-right">
                          <a href="{{ route('bank.index') }}" class="btn btn-secondary">
                              Batal
                          </a>
                          <button type="submit" class="btn btn-primary">Simpan</button>
                      </div>
                    </div>
                  </form>
              @endif
          </div><!-- /.card-body -->
        </div>
        <!-- /.nav-tabs-custom -->
      </div>
      <!-- /.col -->
    </div>
</div>

<script type="text/javascript">
</script>
@endsection
