@extends('layouts.app')

@section('content')
<div class="container">

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
                Sahabat
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item">
                  <a href="{{ url('/') }}">
                      Home
                  </a>
              </li>
              <li class="breadcrumb-item active">
                  Sahabat
              </li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <div class="row">
      <div class="col-md-12">
        <div class="card card-primary card-outline">
            <div class="card-header">
                <div class="card-tools">
                    <a href="{{ url('/team/create') }}" class="btn btn-primary">
                        <i class="fas fa-plus-circle"></i>&nbsp;&nbsp; Tambah Data
                    </a>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
            @if (session('alert'))
                <div class="alert {!! session('alert') !!} alert-dismissible fade show" role="alert">
                    {!! session('message') !!}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No.</th>
                  <th>Nama</th>
                  <th>No. KTP</th>
                  <th>Email</th>
                  <th>Alamat</th>
                  <th>Saldo</th>
                  <th>Penarikan Pending</th>
                  <th style="width: 20%;"></th>
                </tr>
                </thead>
                <tbody>
                    @for ($i = 0; $i < sizeof($team); $i++)
                        <tr>
                          <td>
                              {{ ($i+1) }}
                          </td>
                          <td>
                              {{ $team[$i]['name'] }}
                          </td>
                          <td>
                              {{ $team[$i]['card_id'] }}
                          </td>
                          <td>
                              {{ $team[$i]['email'] }}
                          </td>
                          <td>
                              {{ $team[$i]['address'] }}
                          </td>
                          <td class="text-right">
                              @php
                                $balance = 0;
                                $withdraw = 0;
                              @endphp
                              @for ($j = 0; $j < sizeof($team[$i]['balance']); $j++)
                                  @php
                                    $balance += intval($team[$i]['balance'][$j]['nominal']);
                                  @endphp
                              @endfor
                              @for ($j = 0; $j < sizeof($team[$i]['withdraw']); $j++)
                                  @php
                                    $withdraw += intval($team[$i]['withdraw'][$j]['nominal']) + intval($team[$i]['withdraw'][$j]['admin']);
                                  @endphp
                              @endfor
                              {{ number_format($balance - $withdraw, 0, '.', ',') }}
                          </td>
                          <td class="text-right">
                              @php
                                $withdraw = 0;
                              @endphp
                              @for ($j = 0; $j < sizeof($team[$i]['withdraw']); $j++)
                                  @if ($team[$i]['withdraw'][$j]['status'] == 2)
                                      @php
                                          $withdraw += intval($team[$i]['withdraw'][$j]['nominal']) + intval($team[$i]['withdraw'][$j]['admin']);
                                      @endphp
                                  @endif
                              @endfor
                              {{ number_format($withdraw, 0, '.', ',') }}
                          </td>
                          <td class="text-center">
                              <a href="{{ url('/team/'.$team[$i]['id'].'/edit') }}" class="btn btn-sm btn-primary">
                                  <i class="fas fa-edit"></i>&nbsp;&nbsp; Ubah
                              </a>
                             <button type="button" class="btn btn-sm btn-danger" onclick="deleteData({{ $team[$i]['id'] }})">
                                 <i class="fas fa-trash-alt"></i> Hapus
                             </button>
                          </td>
                        </tr>
                    @endfor
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
</div>

<script type="text/javascript">

    $('table').DataTable();

    function deleteData(id) {
        Swal.fire({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
          if (result.value) {
              axios.delete('/team/'+id, {})
              .then(function (response) {
                 location.reload();
              })
              .catch(function (error) {
                 Swal.fire(
                     'Alert!',
                     'Paket telah terpakai.',
                     'warning'
                 )
              });
          }
        })
    }
</script>
@endsection
