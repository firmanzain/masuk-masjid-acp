@extends('layouts.app')

@section('content')
<div class="container">

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
                Paket
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item">
                  <a href="{{ url('/') }}">
                      Home
                  </a>
              </li>
              <li class="breadcrumb-item active">
                  Paket
              </li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <div class="row">
      <div class="col-md-12">
        <div class="card card-primary card-outline">
            <div class="card-header">
                <div class="card-tools">
                    <a href="{{ url('/package/create') }}" class="btn btn-primary">
                        <i class="fas fa-plus-circle"></i>&nbsp;&nbsp; Tambah Data
                    </a>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
            @if (session('alert'))
                <div class="alert {!! session('alert') !!} alert-dismissible fade show" role="alert">
                    {!! session('message') !!}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No.</th>
                  <th>Nama Paket</th>
                  <th>Jumlah Device</th>
                  <th>Lama Bulan</th>
                  <th>Harga</th>
                  <th style="width: 20%;"></th>
                </tr>
                </thead>
                <tbody>
                    @for ($i = 0; $i < sizeof($package); $i++)
                        <tr>
                          <td>
                              {{ ($i+1) }}
                          </td>
                          <td>
                              {{ $package[$i]['name'] }}
                          </td>
                          <td>
                              {{ ($package[$i]['count'] != 0) ? number_format($package[$i]['count'], 0, '.', ',') : 'Tidak terbatas' }}
                          </td>
                          <td>
                              {{ number_format($package[$i]['month'], 0, '.', ',') }}
                          </td>
                          <td>
                              {{ number_format($package[$i]['price'], 0, '.', ',') }}
                          </td>
                          <td class="text-center">
                              <a href="{{ url('/package/'.$package[$i]['id'].'/edit') }}" class="btn btn-primary">
                                  <i class="fas fa-edit"></i>&nbsp;&nbsp; Ubah
                              </a>
                             <button type="button" class="btn btn-danger" onclick="deleteData({{ $package[$i]['id'] }})">
                                 <i class="fas fa-trash-alt"></i> Hapus
                             </button>
                          </td>
                        </tr>
                    @endfor
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
</div>

<script type="text/javascript">

    $('table').DataTable();

    function deleteData(id) {
        Swal.fire({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
          if (result.value) {
              axios.delete('/package/'+id, {})
              .then(function (response) {
                 location.reload();
              })
              .catch(function (error) {
                 Swal.fire(
                     'Alert!',
                     'Paket telah terpakai.',
                     'warning'
                 )
              });
          }
        })
    }
</script>
@endsection
