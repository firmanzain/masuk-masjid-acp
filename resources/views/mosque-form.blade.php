@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="card card-primary card-outline">
          <div class="card-header">
          </div><!-- /.card-header -->
          <div class="card-body">
              @if (isset($mosque))
                  <form method="POST" action="{{ route('mosque.updateacp', $mosque->id) }}">
                    @csrf
                    @method('PUT')
                    <div class="form-group row">
                      <label for="number" class="col-sm-2 col-form-label">Nomor</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" name="number" id="number" placeholder="Nomor" value="{{ $mosque->number }}" required readonly>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="name" class="col-sm-2 col-form-label">Nama Masjid</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" name="name" id="name" placeholder="Nama Masjid" value="{{ $mosque->name }}" required>
                      </div>
                    </div>
                    <div class="form-group row">
                        <label for="address" class="col-sm-2 col-form-label">Alamat</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" name="address" id="address" rows="3" placeholder="Alamat" required>{{ $mosque->address }}</textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="province" class="col-sm-2 col-form-label">Provinsi</label>
                        <div class="col-sm-10">
                            <input type="hidden" id="province_real" value="{{ $mosque->province_id }}">
                            <select class="form-control" name="province" id="province" required>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                      <label for="takmir_name" class="col-sm-2 col-form-label">Nama Takmir</label>
                      <div class="col-sm-10">
                        <input type="hidden" name="takmir_id" value="{{ $mosque->takmir[0]->id }}" readonly>
                        <input type="text" class="form-control" name="takmir_name" id="takmir_name" placeholder="Nama Takmir" value="{{ $mosque->takmir[0]->name }}" required>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="username" class="col-sm-2 col-form-label">Username</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" name="username" id="username" placeholder="Username" value="{{ $mosque->takmir[0]->username }}" required readonly>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="password" class="col-sm-2 col-form-label">Password</label>
                      <div class="col-sm-10">
                        <input type="password" class="form-control" name="password" id="password" placeholder="Password">
                        <small class="form-text text-muted">
                            Kosongkan jika tidak diganti.
                        </small>
                      </div>
                    </div>
                    <div class="form-group row">
                        <label for="package_id" class="col-sm-2 col-form-label">Jenis Paket</label>
                        <div class="col-sm-10">
                            <input type="hidden" id="package_id_real" value="{{ $mosque->package_id }}">
                            <select class="form-control" name="package_id" id="package_id" required>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="team_id" class="col-sm-2 col-form-label">Sahabat</label>
                        <div class="col-sm-10">
                            <input type="hidden" id="team_id_real" value="{{ $mosque->team_id }}">
                            <select class="form-control" name="team_id" id="team_id" required>
                            </select>
                        </div>
                    </div>
                    <!-- <div class="form-group row">
                        <label for="last_subscribe" class="col-sm-2 col-form-label">Tanggal Terakhir Berlangganan</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control datepicker" name="last_subscribe" id="last_subscribe" placeholder="Tanggal Terakhir Berlangganan" value="{{ date('m/d/Y', strtotime($mosque->last_subscribe)) }}" required>
                        </div>
                    </div> -->
                    <div class="form-group row">
                      <div class="col-sm-12 text-right">
                          <a href="{{ route('mosque.index') }}" class="btn btn-secondary">
                              Batal
                          </a>
                          <button type="submit" class="btn btn-primary">Simpan</button>
                      </div>
                    </div>
                  </form>
              @else
                  <form method="POST" action="{{ route('mosque.store') }}">
                    @csrf
                    @method('POST')
                    <div class="form-group row">
                      <label for="name" class="col-sm-2 col-form-label">Nama Masjid</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" name="name" id="name" placeholder="Nama Masjid" required>
                      </div>
                    </div>
                    <div class="form-group row">
                        <label for="address" class="col-sm-2 col-form-label">Alamat</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" name="address" id="address" rows="3" placeholder="Alamat" required></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="province" class="col-sm-2 col-form-label">Provinsi</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="province" id="province" required>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                      <label for="takmir_name" class="col-sm-2 col-form-label">Nama Takmir</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" name="takmir_name" id="takmir_name" placeholder="Nama Takmir" required>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="username" class="col-sm-2 col-form-label">Username</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" name="username" id="username" placeholder="Username" required>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="password" class="col-sm-2 col-form-label">Password</label>
                      <div class="col-sm-10">
                        <input type="password" class="form-control" name="password" id="password" placeholder="Password" required>
                      </div>
                    </div>
                    <div class="form-group row">
                        <label for="package_id" class="col-sm-2 col-form-label">Jenis Paket</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="package_id" id="package_id" required>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="team_id" class="col-sm-2 col-form-label">Sahabat</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="team_id" id="team_id" required>
                            </select>
                        </div>
                    </div>
                    <!-- <div class="form-group row">
                        <label for="last_subscribe" class="col-sm-2 col-form-label">Tanggal Terakhir Berlangganan</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control datepicker" name="last_subscribe" id="last_subscribe" placeholder="Tanggal Terakhir Berlangganan" value="" required>
                        </div>
                    </div> -->
                    <div class="form-group row">
                      <div class="col-sm-12 text-right">
                          <a href="{{ route('mosque.index') }}" class="btn btn-secondary">
                              Batal
                          </a>
                          <button type="submit" class="btn btn-primary">Simpan</button>
                      </div>
                    </div>
                  </form>
              @endif
          </div><!-- /.card-body -->
        </div>
        <!-- /.nav-tabs-custom -->
      </div>
      <!-- /.col -->
    </div>
</div>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">

    $(".datepicker").datepicker();

    function checkPrice(event) {
        let formatted = numeral(event.target.value).format('0,0')
        document.getElementById(event.target.id).value = formatted
    }

    searchProvince();
    searchPackage();
    searchTeam();

    function searchProvince() {
        $("#province").select2();
        $("#province").select2("destroy");
        $("#province").empty();
        axios.get('/province/select', {})
        .then(function (response) {
            let idSelect = 0;
            if ($("#province_real").val()) {
                idSelect = $("#province_real").val();
            }
            for (var i = 0; i < response.data.length; i++) {
                let selected = '';
                if (idSelect != 0) {
                    if (idSelect == response.data[i].id) {
                        selected = 'selected';
                    }
                }
                $("#province").append('<option id="province-'+response.data[i].id+'" value="'+response.data[i].id+'" '+selected+'> '+response.data[i].name+' </option>');
            }
            $("#province").select2();
        })
        .catch(function (error) {
        });
    }

    function searchPackage() {
        $("#package_id").select2();
        $("#package_id").select2("destroy");
        $("#package_id").empty();
        axios.get('/package/select', {})
        .then(function (response) {
            let idSelect = 0;
            if ($("#package_id_real").val()) {
                idSelect = $("#package_id_real").val();
            }
            for (var i = 0; i < response.data.length; i++) {
                let selected = '';
                if (idSelect != 0) {
                    if (idSelect == response.data[i].id) {
                        selected = 'selected';
                    }
                }
                $("#package_id").append('<option id="package-'+response.data[i].id+'" value="'+response.data[i].id+'" '+selected+'> '+response.data[i].name+' (Rp. '+numeral(response.data[i].price).format('0,0')+') </option>');
            }
            $("#package_id").select2();
        })
        .catch(function (error) {
        });
    }

    function searchTeam() {
        $("#team_id").select2();
        $("#team_id").select2("destroy");
        $("#team_id").empty();
        $("#team_id").append('<option id="team-0" value="0" selected> Tidak Ada </option>');
        axios.get('/team/select', {})
        .then(function (response) {
            let idSelect = 0;
            if ($("#team_id_real").val()) {
                idSelect = $("#team_id_real").val();
            }
            for (var i = 0; i < response.data.length; i++) {
                let selected = '';
                if (idSelect != 0) {
                    if (idSelect == response.data[i].id) {
                        selected = 'selected';
                    }
                }
                $("#team_id").append('<option id="team-'+response.data[i].id+'" value="'+response.data[i].id+'" '+selected+'> '+response.data[i].name+' ('+response.data[i].card_id+') </option>');
            }
            $("#team_id").select2();
        })
        .catch(function (error) {
        });
    }
</script>
@endsection
