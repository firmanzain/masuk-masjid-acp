@extends('layouts.app')

@section('content')
<div class="container">

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
                Sahabat
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item">
                  <a href="{{ url('/') }}">
                      Home
                  </a>
              </li>
              <li class="breadcrumb-item active">
                  Sahabat
              </li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <div class="row">
      <div class="col-md-12">
        <div class="card card-primary card-outline">
            <div class="card-header">
                <div class="card-tools">
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
            @if (session('alert'))
                <div class="alert {!! session('alert') !!} alert-dismissible fade show" role="alert">
                    {!! session('message') !!}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No.</th>
                  <th>Tanggal</th>
                  <th>Nama</th>
                  <th>No. KTP</th>
                  <th>Nominal</th>
                  <th>Admin</th>
                  <th>Keterangan</th>
                  <th>Status</th>
                  <th style="width: 20%;"></th>
                </tr>
                </thead>
                <tbody>
                    @for ($i = 0; $i < sizeof($withdraw); $i++)
                        <tr>
                          <td>
                              {{ ($i+1) }}
                          </td>
                          <td>
                              {{ date('d/m/Y', strtotime($withdraw[$i]['date'])) }}
                          </td>
                          <td>
                              {{ $withdraw[$i]['team']['name'] }}
                          </td>
                          <td>
                              {{ $withdraw[$i]['team']['card_id'] }}
                          </td>
                          <td class="text-right">
                              {{ number_format($withdraw[$i]['nominal'], 0, '.', ',') }}
                          </td>
                          <td class="text-right">
                              {{ number_format($withdraw[$i]['admin'], 0, '.', ',') }}
                          </td>
                          <td>
                              {{ $withdraw[$i]['information'] }}
                          </td>
                          <td>
                              @if ($withdraw[$i]['status'] == 1)
                                  <small class="badge badge-success">
                                      Disetujui
                                  </small>
                              @elseif ($withdraw[$i]['status'] == 2)
                                  <small class="badge badge-warning">
                                      Pending
                                  </small>
                              @else
                                  <small class="badge badge-danger">
                                      Ditolak
                                  </small>
                              @endif
                          </td>
                          <td class="text-center">
                              @if ($withdraw[$i]['status'] == 2)
                                  <button type="button" class="btn btn-sm btn-success" onclick="acceptWithdraw({{ $withdraw[$i]['id'] }})">
                                      <i class="fas fa-check-circle"></i> Setujui
                                  </button>
                                  <button type="button" class="btn btn-sm btn-danger" onclick="declineWithdraw({{ $withdraw[$i]['id'] }})">
                                      <i class="fas fa-ban"></i> Tolak
                                  </button>
                              @endif
                          </td>
                        </tr>
                    @endfor
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
</div>

<script type="text/javascript">

    $('table').DataTable();

    function acceptWithdraw(id) {
        Swal.fire({
          title: 'Apakah anda yakin?',
          text: "Pengajuan penarikan akan disetujui!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Ya!'
        }).then((result) => {
          if (result.value) {
              axios.post('/withdraw/accept/'+id, {})
              .then(function (response) {
                 location.reload();
              })
              .catch(function (error) {
                 Swal.fire(
                     'Alert!',
                     'Gagal menyetujui.',
                     'warning'
                 )
              });
          }
        })
    }

    function declineWithdraw(id) {
        Swal.fire({
          title: 'Apakah anda yakin?',
          text: "Pengajuan penarikan akan ditolak!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Ya!'
        }).then((result) => {
          if (result.value) {
              axios.post('/withdraw/decline/'+id, {})
              .then(function (response) {
                 location.reload();
              })
              .catch(function (error) {
                 Swal.fire(
                     'Alert!',
                     'Gagal Menolak.',
                     'warning'
                 )
              });
          }
        })
    }
</script>
@endsection
