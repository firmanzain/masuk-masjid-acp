@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="card card-primary card-outline">
          <div class="card-header">
          </div><!-- /.card-header -->
          <div class="card-body">
              @if (isset($team))
                  <form method="POST" action="{{ route('team.update', $team->id) }}" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="form-group row">
                      <label for="name" class="col-sm-2 col-form-label">Nama</label>
                      <div class="col-sm-4">
                        <input type="text" class="form-control" name="name" id="name" placeholder="Nama" value="{{ $team->name }}" required>
                      </div>
                      <label for="card_id" class="col-sm-2 col-form-label">No. KTP</label>
                      <div class="col-sm-4">
                        <input type="text" class="form-control" name="card_id" id="card_id" placeholder="No. KTP" value="{{ $team->card_id }}" required>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="born_place" class="col-sm-2 col-form-label">Tempat Lahir</label>
                      <div class="col-sm-4">
                        <input type="text" class="form-control" name="born_place" id="born_place" placeholder="Tempat Lahir" value="{{ $team->born_place }}" required>
                      </div>
                      <label for="born_date" class="col-sm-2 col-form-label">Tanggal Lahir</label>
                      <div class="col-sm-4">
                        <input type="text" class="form-control datepicker" name="born_date" id="born_date" placeholder="Tanggal Lahir" value="{{ date('m/d/Y', strtotime($team->born_date)) }}" required>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="email" class="col-sm-2 col-form-label">Email</label>
                      <div class="col-sm-4">
                        <input type="text" class="form-control" name="email" id="email" placeholder="Email" value="{{ $team->email }}" required>
                      </div>
                      <label for="password" class="col-sm-2 col-form-label">Password</label>
                      <div class="col-sm-4">
                          <input type="password" class="form-control" name="password" id="password" placeholder="Password">
                          <small class="form-text text-muted">
                              Kosongkan jika tidak diganti.
                          </small>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="occupation" class="col-sm-2 col-form-label">Pekerjaan</label>
                      <div class="col-sm-4">
                        <input type="text" class="form-control" name="occupation" id="occupation" placeholder="Pekerjaan" value="{{ $team->occupation }}" required>
                      </div>
                      <label for="image" class="col-sm-2 col-form-label">Foto</label>
                      <div class="col-sm-4">
                          <img src="{{ $team->image }}" class="rounded mx-auto d-block mb-2" alt="" style="width: 50%;">
                          <input type="file" class="form-control" name="image" id="image" placeholder="Foto">
                          <small class="form-text text-muted">
                              Kosongkan jika tidak diganti.
                          </small>
                      </div>
                    </div>
                    <div class="form-group row">
                        <label for="address" class="col-sm-2 col-form-label">Alamat</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" name="address" id="address" rows="3" placeholder="Alamat" required>{{ $team->address }}</textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-sm-12 text-right">
                          <a href="{{ route('team.index') }}" class="btn btn-secondary">
                              Batal
                          </a>
                          <button type="submit" class="btn btn-primary">Simpan</button>
                      </div>
                    </div>
                  </form>
              @else
                  <form method="POST" action="{{ route('team.store') }}" enctype="multipart/form-data">
                    @csrf
                    @method('POST')
                    <div class="form-group row">
                      <label for="name" class="col-sm-2 col-form-label">Nama</label>
                      <div class="col-sm-4">
                        <input type="text" class="form-control" name="name" id="name" placeholder="Nama" required>
                      </div>
                      <label for="card_id" class="col-sm-2 col-form-label">No. KTP</label>
                      <div class="col-sm-4">
                        <input type="text" class="form-control" name="card_id" id="card_id" placeholder="No. KTP" required>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="born_place" class="col-sm-2 col-form-label">Tempat Lahir</label>
                      <div class="col-sm-4">
                        <input type="text" class="form-control" name="born_place" id="born_place" placeholder="Tempat Lahir" required>
                      </div>
                      <label for="born_date" class="col-sm-2 col-form-label">Tanggal Lahir</label>
                      <div class="col-sm-4">
                        <input type="text" class="form-control datepicker" name="born_date" id="born_date" placeholder="Tanggal Lahir" required>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="email" class="col-sm-2 col-form-label">Email</label>
                      <div class="col-sm-4">
                        <input type="text" class="form-control" name="email" id="email" placeholder="Email" required>
                      </div>
                      <label for="password" class="col-sm-2 col-form-label">Password</label>
                      <div class="col-sm-4">
                          <input type="password" class="form-control" name="password" id="password" placeholder="Password" required>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="occupation" class="col-sm-2 col-form-label">Pekerjaan</label>
                      <div class="col-sm-4">
                        <input type="text" class="form-control" name="occupation" id="occupation" placeholder="Pekerjaan" required>
                      </div>
                      <label for="image" class="col-sm-2 col-form-label">Foto</label>
                      <div class="col-sm-4">
                          <input type="file" class="form-control" name="image" id="image" placeholder="Foto" required>
                      </div>
                    </div>
                    <div class="form-group row">
                        <label for="address" class="col-sm-2 col-form-label">Alamat</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" name="address" id="address" rows="3" placeholder="Alamat" required></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-sm-12 text-right">
                          <a href="{{ route('team.index') }}" class="btn btn-secondary">
                              Batal
                          </a>
                          <button type="submit" class="btn btn-primary">Simpan</button>
                      </div>
                    </div>
                  </form>
              @endif
          </div><!-- /.card-body -->
        </div>
        <!-- /.nav-tabs-custom -->
      </div>
      <!-- /.col -->
    </div>
</div>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">

    $(".datepicker").datepicker();

</script>
@endsection
